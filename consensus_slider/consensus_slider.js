// JavaScript Document

/*
*	Time slider
* 
*	Jquery Slider used to "go back in time".  The user can use the slider to
* see the sequence of changes taking place on the page.
*/

Drupal.behaviors.consensus.prototype.timeSlider = function () {
	
	//check that the slider exists
	if ($('#c_slider_element').length == 0){
		return false;
	}
	
	// create an array/object to store the timstamp/id pairs of all revision items
	var revisionTime = Array();
	var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
	
	for (var i in revisionIds) {
		var time = $('#'+revisionIds[i]).attr('timestamp');
		if (!(revisionTime[time] instanceof Array)) {
			revisionTime[time] = Array();
		}
		revisionTime[time].push(revisionIds[i]);
	}
	
	// create array of timestamp values
	var timestamp = Array();
	var i=0; 
	for (time in revisionTime) {
		timestamp[i] = time;
		i++
	}
	timestamp = timestamp.sort();
	
	// set orientation of the bar
	var direction = 'horizontal';
	if (Drupal.settings.consensus_slider.orientation != null) {
		direction = Drupal.settings.consensus_slider.orientation;
	}
	
	// set the value of current time
	$('#c_slider_value .time_label').html(Drupal.t('Last edited on:'));
	$('#c_slider_value .time').html(Drupal.settings.consensus_slider.timestamps[timestamp[timestamp.length-1]]);
	
	var startValue = 0;
	$('#c_slider_element').slider({
		//handle: '#slider-handle',
		min: 0,
		max: timestamp.length-1,
		value: timestamp.length-1,
		orientation: direction,
		
		animate: true,
		//values: timestamp,
		//values: [1,2,3,4],
		start: function(e,ui){
			startValue = ui.value;
			$('#c_slider_value .time_label').html(Drupal.t('Page at'));
		},  
		stop: function(e,ui){  
		},  
		slide: function(e,ui){
			var current = timestamp[ui.value];
			for (time in revisionTime) {
				if (time <= current) {
					for (key in revisionTime[time]) {
						var revision =  $('#'+revisionTime[time][key]);
						revision.removeClass('after_slider').addClass('before_slider');
						revision.closest('.consensus_item').removeClass('after_slider').addClass('before_slider');
					}
				} else {
					for (key in revisionTime[time]) {	
						var revision =  $('#'+revisionTime[time][key]);
						revision.removeClass('before_slider').addClass('after_slider');
						if (revision.siblings('.revision').length == revision.siblings('.after_slider').length ) {
							revision.closest('.consensus_item').removeClass('before_slider').addClass('after_slider');
						}
						
					}
				}
			}
							
			$('#c_slider_value .time').html(Drupal.settings.consensus_slider.timestamps[current]);
			
		},
	});
}
