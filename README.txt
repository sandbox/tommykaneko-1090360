
-- SUMMARY --

Consensus is a suite of tools that allows the editing of nodes directly in the page.

The process of editing is structured in such a way as to enable debate.  The process is an interpretation of the Formal Consensus Decision Making Process: http://www.consensus.net

For up to date instructions, visit the project page.

-- REQUIREMENTS -- 

* jQuery update module, version 6.x-2.0-alpha1 or higher is required:
	http://drupal.org/project/jquery_update

* Consensus uses the jQuery v1.4.2 library, so this will have to be downloaded too:
	http://docs.jquery.com/Downloading_jQuery

* jQuery UI module, it must be the 6.x-1.4 branch:
	http://drupal.org/node/906160



-- INSTALLATION --

* Download and enable the jQuery Update module - you will need 6.x-2.0-alpha1 or higher:
	http://drupal.org/project/jquery_update

	* You will need to install jQuery v1.4.2 from here:
			http://docs.jquery.com/Downloading_jQuery
	
	* Put the downloaded jquery file into the module directory, and rename as follows:
			/sites/all/modules/jquery_update/replace/jquery.js

* Download the jQuery UI module from here, it must be the 6.x-1.4 branch:
		http://drupal.org/node/906160

	* Apply the patch to jQuery UI from here:
		http://drupal.org/node/749126#comment-3411750
	
	* You will need to install jQuery UI v1.8+ from here:
			http://jqueryui.com/download
	
	* Put the downloaded archive into the module directory:
			/sites/all/modules/jquery_ui/jquery.ui-1.8.x.zip
	
	* Extract the archive.  This will create the following sub-directory:
			/sites/all/modules/jquery_ui/jquery.ui-1.8.x/
	
	* Rename the sub-directory into "jquery.ui" within the jquery_ui module folder:
			/sites/all/modules/jquery_ui/jquery.ui/
	
		*  so the actual jQuery UI JavaScript files are located in:
			/sites/all/modules/jquery_ui/jquery.ui/ui/*.js

	* If you have installation issues, have a look at this thread:
			http://drupal.org/node/749126

* Enable the jQuery UI module


* Enable the Consensus module

* Create a new content type in /admin/content/types , and select the option to "Consensify this node type".  This content type will now be in a Consensus format.  You may convert an existing content type to the Consensus format by selecting this option, but this is not recommended, as it is a destructive process, and there may be no way to convert the node back.

* Consensus content types do not work with some input filters, such as HTML filter.  These filters must be disabled for this content type.

* Check Consensus Settings - /admin/settings/consensus