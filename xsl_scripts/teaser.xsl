<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template match="/">
<xsl:apply-templates select="/consensus/content"/>  
<xsl:apply-templates select="/document/content"/>  
</xsl:template>


<xsl:template match="heading">
	<h2>
    <xsl:value-of select="."/>
  </h2>
</xsl:template>


<xsl:template match="content">
	<div>
		<xsl:attribute name="id">c_n<xsl:value-of select="/document/issue/id"/></xsl:attribute>
		<xsl:attribute name="class">c_content</xsl:attribute>
		<xsl:apply-templates/>
    <br/>
	</div>
</xsl:template>


<xsl:template match="item">    
  <div>
    <xsl:call-template name="populate-item"/>
  </div>
</xsl:template>


<xsl:template name="populate-item">
    <xsl:attribute name="id">c_n<xsl:value-of select="/document/issue/id"/>item<xsl:value-of select="@id"/></xsl:attribute>
    <!-- set the class names -->
    <xsl:attribute name="class">c_<xsl:value-of select="revision[@current='1']/attribute::type"/> consensus_item<xsl:if test="revision[@current=1]/attribute::parentid"> parentid<xsl:value-of select="revision[@current='1']/attribute::parentid"/> level</xsl:if><xsl:if test="hide[@current='1']/attribute::action = 'hide'"> c_hide</xsl:if><xsl:if test="item/revision[@current='1']/attribute::type = 'block'"> c_blocked</xsl:if></xsl:attribute>
    <!--
    <xsl:for-each select="child::revision">
      <xsl:sort select="@timestamp" data-type="number" order="descending"/>
    	<xsl:apply-templates select="."/>
    </xsl:for-each>
    -->
    <xsl:apply-templates select="child::revision[last()]">
    </xsl:apply-templates>

    <xsl:if test="child::item">
            <xsl:apply-templates select="child::item"/>
    </xsl:if>                

</xsl:template>

<!-- Transform the revision elements to readable HTML, adding classes to allow filtering. -->
<xsl:template match="revision">
  <xsl:if test="@current=1">
      
     <!-- finally, put the text into the div -->
    <xsl:if test="@type='heading'"> 
      <h2><xsl:value-of select="text"/></h2>
    </xsl:if>
    <xsl:if test="@type!='heading'"> 
      <p><xsl:copy-of select="text/node()"/></p>
    </xsl:if>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>