<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>


<xsl:template match="/">
<!-- <div id="issue"><xsl:apply-templates select="/document/issue" /></div> -->
<xsl:apply-templates select="/consensus/content"/>  
<!-- for legacy systems: -->
<xsl:apply-templates select="/document/content"/>  

<!-- for returning individual items -->
<xsl:apply-templates select="/endorse"/>  
<xsl:apply-templates select="/item"/>  

</xsl:template>
<!--
<xsl:template match="issue">
<div id="c_issue_data">
	<a name="creation_time"><xsl:value-of select="creation_time"/></a>
	<a name="modified_time"><xsl:value-of select="modified_time"/></a>
    <div id="c_issue_creator">
    	<a name="user_id"><xsl:value-of select="creator/user/id"/></a>
    	<a name="user"><xsl:value-of select="creator/user/name"/></a>
    </div>
</div>
</xsl:template>
-->

<xsl:template match="heading">
	<h2>
    <xsl:value-of select="."/>
  </h2>
</xsl:template>


<xsl:template match="content">
	<div>
		<xsl:attribute name="id">c_n<xsl:value-of select="/document/issue/id"/><xsl:value-of select="/consensus/issue/id"/></xsl:attribute>
		<xsl:attribute name="class">c_content</xsl:attribute>
		<ul>
      <xsl:apply-templates/>
    </ul>
    <br/>
	</div>
</xsl:template>


<xsl:template match="item">    
  <li>
    <xsl:call-template name="populate-item"/>
  </li>
</xsl:template>


<xsl:template name="populate-item">
    <xsl:attribute name="id">c_n<xsl:value-of select="/document/issue/id"/><xsl:value-of select="/consensus/issue/id"/>item<xsl:value-of select="@id"/></xsl:attribute>
    <!-- set the class names -->
    <xsl:attribute name="class">c_<xsl:value-of select="revision[@current='1']/attribute::type"/> consensus_item<xsl:if test="revision[@current=1]/attribute::parentid = string(.)"> parentid<xsl:value-of select="revision[@current='1']/attribute::parentid"/> level</xsl:if><xsl:if test="hide[@current='1']/attribute::action = 'hide'"> c_hide</xsl:if><xsl:if test="item/revision[@current='1']/attribute::type = 'block'"> c_blocked</xsl:if><xsl:if test="count(revision/endorse[@active='1']) = 0"> c_unendorsed</xsl:if></xsl:attribute>
    <!--
    <xsl:for-each select="child::revision">
      <xsl:sort select="@timestamp" data-type="number" order="descending"/>
    	<xsl:apply-templates select="."/>
    </xsl:for-each>
    -->
    <xsl:for-each select="revision">
      <div>
        <xsl:attribute name="id">i<xsl:value-of select="../@id" />r<xsl:value-of select="@revid" /></xsl:attribute>
        <xsl:attribute name="timestamp"><xsl:value-of select="@timestamp"/></xsl:attribute>
        <xsl:attribute name="class">revision <xsl:if test="@current=1">current top</xsl:if> c_<xsl:value-of select="@type" /></xsl:attribute>
        <xsl:if test="@revertTo">
          <xsl:variable name="revertId" select="@revertTo"/>
          <xsl:attribute name="class">revision <xsl:if test="@current=1">current top</xsl:if> c_<xsl:value-of select="preceding-sibling::revision[@revid=$revertId]/@type" /></xsl:attribute>
          <xsl:apply-templates select="preceding-sibling::revision[@revid=$revertId]"/>
        </xsl:if>
        
        <xsl:if test="not(@reverTo)">
          <xsl:apply-templates select="."/>
        </xsl:if>
      </div>
		</xsl:for-each>
    
    <xsl:if test="child::item">
    	<ul>
      	<xsl:apply-templates select="child::item"/>
      </ul>
    </xsl:if>                

</xsl:template>

<!-- Transform the revision elements to readable HTML, adding classes to allow filtering. -->
<xsl:template match="revision">
    <div class="slide_wrapper upper">
      <xsl:if test="extra">
        <xsl:copy-of select="extra/*" />
      </xsl:if>
    </div>
  <!-- put extra data into invisible divs -->
    <div class="data" style="display:none;">
        <div class="data-type">
          <xsl:value-of select="@type"/>
        </div>
        <div class="data-author">
        	<xsl:attribute name="data-uid"><xsl:value-of select="author/user/id"/></xsl:attribute>
          <xsl:attribute name="data-name"><xsl:value-of select="author/user/name"/></xsl:attribute>
				</div>
            <xsl:apply-templates select="endorse"/> 
          <xsl:if test="reason">
          <div class="data-reason"><xsl:value-of select="reason"/></div>
          </xsl:if>
          <xsl:for-each select="revert">
          <div class="data-revert">	
            <xsl:attribute name="timestamp">
            <xsl:value-of select="@timestamp"/>
          </xsl:attribute>
            <span class="data-uid"><xsl:value-of select="user/id"/></span>
            <span class="data-name"><xsl:value-of select="user/name"/></span>
          </div>
          </xsl:for-each>
          <xsl:for-each select="notes">
          <div class="data-notes">	
            <xsl:value-of select="."/>
          </div>
              </xsl:for-each>
          </div>
      
     <!-- finally, put the text into the div -->
    
    <xsl:if test="@type='heading'"> 
      <xsl:choose>
        <xsl:when test="../../../../revision[@type='heading']"> 
          <div class="data-text"><h5><xsl:attribute name="class"><xsl:if test="not(@current=1)">hidden_heading</xsl:if></xsl:attribute><xsl:value-of select="text"/></h5></div>
        </xsl:when> 
        <xsl:when test="../../../revision[@type='heading']"> 
          <div class="data-text"><h4><xsl:attribute name="class"><xsl:if test="not(@current=1)">hidden_heading</xsl:if></xsl:attribute><xsl:value-of select="text"/></h4></div>
        </xsl:when> 
        <xsl:when test="../../revision[@type='heading']"> 
          <div class="data-text"><h3><xsl:attribute name="class"><xsl:if test="not(@current=1)">hidden_heading</xsl:if></xsl:attribute><xsl:value-of select="text"/></h3></div>
        </xsl:when> 
        <xsl:otherwise> 
          <div class="data-text"><h2><xsl:attribute name="class"><xsl:if test="not(@current=1)">hidden_heading</xsl:if></xsl:attribute><xsl:value-of select="text"/></h2></div>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    <xsl:if test="@type='reference'"> 
    <div class="data-text">
      <xsl:copy-of select="text/node()"/>
    </div>
    </xsl:if>
    <xsl:if test="@type!='heading' and @type!='reference'"> 
    <div class="data-text">
      <xsl:call-template name="PreserveLineBreaks">
              <xsl:with-param name="text" select="text"/>
          </xsl:call-template><!--<xsl:copy-of select="text/node()"/>-->
    </div>
    </xsl:if>
</xsl:template>


<xsl:template name="PreserveLineBreaks">
  <xsl:param name="text"/>
  <xsl:choose>
      <xsl:when test="contains($text,'&#xA;')">
          <xsl:value-of select="substring-before($text,'&#xA;')"/>
          <br/>
          <xsl:call-template name="PreserveLineBreaks">
              <xsl:with-param name="text">
                  <xsl:value-of select="substring-after($text,'&#xA;')"/>
              </xsl:with-param>
          </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:value-of select="$text"/>
      </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="endorse" match="endorse">
<xsl:if test="@active!=0">
<div class="data-endorse">
  <xsl:attribute name="timestamp"><xsl:value-of select="@timestamp"/></xsl:attribute>
  <xsl:attribute name="data-uid"><xsl:value-of select="user/id"/></xsl:attribute>
  <xsl:attribute name="data-name"><xsl:value-of select="user/name"/></xsl:attribute>
	<xsl:apply-templates select="context"/>
</div>
</xsl:if>
</xsl:template>


<xsl:template match="context">
  <span class="data-context">
    <xsl:attribute name="data-action"><xsl:value-of select="@action"/></xsl:attribute>
    <xsl:attribute name="data-parentId"><xsl:value-of select="@parentId"/></xsl:attribute>
    <xsl:attribute name="data-parentRevId"><xsl:value-of select="@parentRevId"/></xsl:attribute>
    <xsl:attribute name="data-timestamp"><xsl:value-of select="@timestamp"/></xsl:attribute>
  </span>
</xsl:template>

 </xsl:stylesheet>