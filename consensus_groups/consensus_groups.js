// JavaScript Document

Drupal.behaviors.consensus.prototype.userGroup = Array();
// array of groups with array of UserIDs in each
Drupal.behaviors.consensus.prototype.groupUser = Array();

/**
 *	Initiate functions when document ready
 */
Drupal.behaviors.consensus.prototype.initiateGroups = function (context) {
	var groups_block = $('.c_group');
	if (Drupal.settings.consensus_users != null && groups_block.length != 0) {
		// set variables
		Drupal.behaviors.consensus.prototype.setGroupIds();
		Drupal.behaviors.consensus.prototype.setGroupUser();
		
		// update the group
		var groups = Drupal.behaviors.consensus.prototype.groupIds;
		for (i=0; i<groups.length;i++) {
			Drupal.behaviors.consensus.prototype.updateGroupInclude(groups[i]);
		}
		Drupal.behaviors.consensus.prototype.highlightGroupItems(context);
		Drupal.behaviors.consensus.prototype.toggleInclude(context);
		Drupal.behaviors.consensus.prototype.clickGroup(context);
		// when page initialises, perform action
		for (i=0; i<groups.length;i++) {
			Drupal.behaviors.consensus.prototype.includeGroupHighlight('#'+groups[i], context);
		}
	}
}




/**
 *	bind checkbox clicks to toggleGroupInclude function
 */
Drupal.behaviors.consensus.prototype.toggleInclude = function (context) {
	// when page initialises, perform action
	$('.c_group input.c_include', context).click(function (event) {
		Drupal.behaviors.consensus.prototype.includeGroupHighlight(this);
		Drupal.behaviors.consensus.prototype.toggleGroupInclude(this);
	});

}

/**
 *
 */
Drupal.behaviors.consensus.prototype.clickGroup = function (context) {
	// if the context is not the document, return false
	if ($('.c_content', context).length == 0) {
		return false;
	}
	$('#consensus-groups').click(function(e) {
			 var clicked = $(e.target);
			if (clicked.closest('li.c_group').length != 0) {
				if (clicked.closest('a, input, label').length == 0) {
					var target = clicked.closest('li.c_group');
					Drupal.behaviors.consensus.prototype.collapsibleItems(target);
				}
			}
	});
}

/**
 *	update the condition of the group include checkboxes to reflect included users
 */
Drupal.behaviors.consensus.prototype.updateGroupInclude = function (groupId, context) {	
	var includedUsers = Array(); 
	
	for (var i in Drupal.behaviors.consensus.prototype.groupUser[groupId]) {
		var uid = Drupal.behaviors.consensus.prototype.groupUser[groupId][i];
		if ($('#'+uid+' input.c_include').is(':checked')) {
			includedUsers.push(uid);
		}
	}
	
	if (includedUsers.length == Drupal.behaviors.consensus.prototype.groupUser[groupId].length) {
		$('#'+groupId+' input.c_include').attr('checked', true);
	} else if (includedUsers.length == 0) {
		$('#'+groupId+' input.c_include').attr('checked', false);
	}
}



Drupal.behaviors.consensus.prototype.toggleGroupInclude = function (target) {
	
	target = $(target).closest('.c_group');
	var ogid = target.attr('id');
	var checked = target.find('input.c_include').is(':checked');
	
	// update group include information
	Drupal.behaviors.consensus.prototype.includeGroupHighlight('#'+ogid);
	
	var groupUser = Drupal.behaviors.consensus.prototype.groupUser;

	// include the group members of the included group
	if (checked) {
		
		for (var i in groupUser[ogid]) {
			$('#consensus-users #'+groupUser[ogid][i]).find('.c_include').attr('checked', true);
			//Drupal.behaviors.consensus.prototype.updateContribution('#consensus-users #'+uid);
			Drupal.behaviors.consensus.prototype.includeHighlight('#consensus-users #'+groupUser[ogid][i]);
		}
	} 
	// exclude the user if he is not member of another included group
	else {
		for (var a in groupUser[ogid]) {
			var uid = groupUser[ogid][a];
			var active = false;
			for (i in Drupal.behaviors.consensus.prototype.userGroup[uid]) {
				if ($('#'+Drupal.behaviors.consensus.prototype.userGroup[uid][i]).find('.c_include').is(':checked')) {
					active = true;
				}
			}
			if (!active) {
				$('#consensus-users #'+uid).find('.c_include').attr('checked', false);
				//Drupal.behaviors.consensus.prototype.updateContribution('#consensus-users #'+uid);
				Drupal.behaviors.consensus.prototype.includeHighlight('#consensus-users #'+uid);
			}
		}
	}
	Drupal.behaviors.consensus.prototype.updateAllContributions();
	Drupal.behaviors.consensus.prototype.includeExclude();

}

/**
 *	Put group ids into cache for quick jQuery retrieval
 */
Drupal.behaviors.consensus.prototype.groupIds = Array();
Drupal.behaviors.consensus.prototype.setGroupIds = function (context) {
	var groups = $('.consensus_groups li.c_group', context);
	for (i=0; i<groups.length;i++) {
		Drupal.behaviors.consensus.prototype.groupIds[i] = groups[i].id;
	}
}

/**
 *	Builds an array of userIDs wth a nested array of GroupIDs each user belongs to
 *
 */
Drupal.behaviors.consensus.prototype.setGroupUser = function() {
	var userGroup = Array();
	var groupUser = Array();
	
	var groups = Drupal.behaviors.consensus.prototype.groupIds;
	
	if (groups.length != 0) {
		
		for (i=0; i<groups.length; i++) {
			var ogid = groups[i];
			var users = $('#'+groups[i]).find('.c_group_data .c_group_data-user');
			for (a=0; a<users.length; a++) {
				var uid = 'uid-'+String($(users[a]).attr('data-uid'));
				
				// set userGroup
				if (!(userGroup[uid] instanceof Array)) {
					userGroup[uid] = Array();
				}
				userGroup[uid].push(ogid);
				
				// set groupUser
				if (!(groupUser[ogid] instanceof Array)) {
					groupUser[ogid] = Array();
				}
				if (typeof(uid) != 'undefined') {
					groupUser[ogid].push(uid);
				}
			}
		}
	}
	Drupal.behaviors.consensus.prototype.userGroup = userGroup;
	Drupal.behaviors.consensus.prototype.groupUser = groupUser;
}



Drupal.behaviors.consensus.prototype.highlightGroupItems = function (context) {
	$('.c_group').hover(function(){
		var users = Drupal.behaviors.consensus.prototype.groupUser[this.id];
		for (var i in users) {
			Drupal.behaviors.consensus.prototype.highlightItems('#'+users[i]);
			//$('.consensus_user #uid-'+uid ).mouseenter();//mousenter for matching category with no .hovered class
			
		}
	}, function() {
		Drupal.behaviors.consensus.prototype.highlightItems(false);
	});
	/*
	// bind click to toggle
	$('li.c_group').click(function(){
		if (!$(this).find('fieldset:first').hasClass('stop_collapse')) {
			var fieldset = $(this).find('fieldset:first')[0];
			Drupal.behaviors.consensus.prototype.toggleFieldset(fieldset);
		} else {
			$(this).find('fieldset:first').removeClass('stop_collapse');
		}
	});
	$('li.c_group a, li.c_group input, li.c_group label').click(function(event) {
			event.stopPropagation();
	})
	*/
};
	
// add class to the user element if include checkbox is on
Drupal.behaviors.consensus.prototype.includeGroupHighlight = function (target, context) {
	target = $(target);
	if (!target.hasClass('c_group')) {
		target = target.closest('.c_group');
	}
	if (target.find('.c_include').is(':checked')) {
		target.addClass('c_highlight_include');
	} else {
		target.removeClass('c_highlight_include');
	}
};

/*
Drupal.behaviors.consensus.prototype.toggleFieldset = function(fieldset) {
	if ($(fieldset).is('.collapsed')) {
		// Action div containers are processed separately because of a IE bug
		// that alters the default submit button behavior.
		var content = $('> div:not(.action)', fieldset);
		$(fieldset).removeClass('collapsed');
		content.hide();
		content.slideDown( {
			duration: 'fast',
			easing: 'linear',
			complete: function() {
				collapseScrollIntoView(this.parentNode);
				this.parentNode.animating = false;
				$('div.action', fieldset).show();
				minimizeVal(fieldset);

			},
			step: function() {
				// Scroll the fieldset into view
				collapseScrollIntoView(this.parentNode);
			}
		});
	}
	else {
		$('div.action', fieldset).hide();
		var content = $('> div:not(.action)', fieldset).slideUp('fast', function() {
			$(this.parentNode).addClass('collapsed');
			this.parentNode.animating = false;
			minimizeVal(fieldset);
		});
	}
};
*/

/*
 *	Makes the users list sortable
 */
 /*
Drupal.behaviors.consensus.prototype.groupsSortable = function () {
	$('ul.consensus_groups').sortable({
			items: 'li.c_group',
			axis: 'y',
			//containment: 'parent',
			//forceHelperSize: true, // may not be necessary
			//forcePlaceholderSize: true,
			//helper: 'original',
			//helper: 'clone', // if the display is better iwth clone
			handle: '.c_group_wrapper',
			//placeholder: 'c_user_placeholder',
			//revert: true,
			tolerance: 'intersect',
			opacity: 0.6,
			//sort:  function(event, ui) {	
			//	console.log('starting');
			//},
			update:  function(event, ui) {	
				ui.item.find('.collapsible').addClass('stop_collapse');
				$('#edit-sort-order').val($(this).sortable('serialize'));
				$('#edit-sortby').val('custom');
			},
	
	});
};

// changes the input tag's value with c_minimize
Drupal.behaviors.consensus.prototype.minimizeVal = function (fieldset) {
	if (!$(fieldset).hasClass('c_user')) {
		target = $(fieldset).closest('.c_user');
	} else {
		target = $(fieldset);
	}
	if (target.find('.collapsible').hasClass('collapsed')) {
		target.find('.c_minimize').val(1);
	} else {
		target.find('.c_minimize').val(0);
	}
}
				
/**
 * Scroll a given fieldset into view as much as possible.
 */
/*
Drupal.behaviors.consensus.prototype.collapseScrollIntoView = function (node) {
	var h = self.innerHeight || document.documentElement.clientHeight || $('body')[0].clientHeight || 0;
	var offset = self.pageYOffset || document.documentElement.scrollTop || $('body')[0].scrollTop || 0;
	var posY = $(node).offset().top;
	var fudge = 55;
	if (posY + node.offsetHeight + fudge > h + offset) {
		if (node.offsetHeight > h) {
			window.scrollTo(0, posY);
		} else {
			window.scrollTo(0, posY + node.offsetHeight - h + fudge);
		}
	}
};


Drupal.behaviors.consensus.prototype.collapse = function (context) {
	$('fieldset.collapsible > legend:not(.collapse-processed)', context).each(function() {
		var fieldset = $(this.parentNode);
		// Expand if there are errors inside
		if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
			fieldset.removeClass('collapsed');
		}

		// Turn the legend into a clickable link and wrap the contents of the fieldset
		// in a div for easier animation
		var text = this.innerHTML;
			$(this).empty().append($('<a href="#">'+ text +'</a>').click(function() {
				var fieldset = $(this).parents('fieldset:first')[0];
				// Don't animate multiple times
				if (!fieldset.animating) {
					fieldset.animating = true;
					toggleFieldset(fieldset);
				}
				return false;
			}))
			.after($('<div class="fieldset-wrapper"></div>')
			.append(fieldset.children(':not(legend):not(.action)')))
			.addClass('collapse-processed');
	});
};
*/
