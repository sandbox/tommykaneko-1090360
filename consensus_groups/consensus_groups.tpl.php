<?php
// $Id
//print drupal_get_form('consensus_users_form');
?>
<div id="consensus-groups">
  <form id="<?php print $form['#id'].'-'.$form['#sorted']; ?>" method="<?php print $form['#method']; ?>" accept-charset="UTF-8" action="<?php print $form['#action']; ?>"><?php
  print drupal_render($form['form_build_id']);
  print drupal_render($form['form_id']);
  print drupal_render($form['form_token']);
  print drupal_render($form['sortby']);
  print drupal_render($form['sort-order']);
	//print theme('consensus_users_sortby', $sortby);
  ?>
  <ul class="consensus_groups">
    <?php
    foreach ($info['groups'] as $id => $group) {
      ?>
    <li id="ogid-<?php print $id;?>" class="c_group"> 
        <div class = "c_group_wrapper">
          <div class="c_group_name"><h3><?php print $group['title'];?> <span class="small"><?php if ($id > 0) {print l(t('more...'),'node/'.$id); } ?></span></h3></div>
        <fieldset class="collapsible<?php if ($info['data']['minimize'][$id] || !isset($info['data']['minimize'][$id])){ ?> collapsed<?php } else { } ?>">
          <legend>More</legend>
        <div class="c_group_info">
        	<!-- Put in group stats -->
          <div><?php print count($group['users']).t(' members on this page') ?></div>
          <ul class="c_items">
            <?php
              foreach ($group['count'] as $type => $count) {
            		if ($count['author']['current'] && $type != 'heading') {?>
            <li class="c_<?php print $type; ?>" title="<?php print $type;?>"><img src="<?php print url(drupal_get_path('module', 'consensus') . '/css/images/bullet-' . $type . '.png');?>" alt="<?php print $type;?>"/>x<?php print $count['author']['current'];?></li>
								<?php }
							} ?>
            <li class="c_authored_endorsed" title="<?php print t('endorsed/author ratio');?>"><img src="<?php print url(drupal_get_path('module', 'consensus') . '/css/images/endorsed-authored.png');?>" alt="<?php print t('endorsed/author ratio');?>"/><?php print $group['count']['endorsed']['all'] ? $group['count']['endorsed']['all'] : 0 ;?>/<?php print $account['count']['authored']['all'] ? $group['count']['authored']['all'] : 0;?></li>
          </ul>
        </div>
        <div class="c_group_data" style="display:none">
          <?php foreach ($group['users'] as $uid => $user)  {?>
          <div class="c_group_data-user" data-uid="<?php print $uid; ?>"></div>
          <?php } ?>
					<!--
					<?php // foreach ($group['items'] as $type => $items) { ?>
						<?php //foreach ($items as $item) { ?>
          <div class="c_group_data-<?php // print $type; ?>">
            <span class="group_data-itemid"><?php // print $item['itemid']; ?></span>
            <span class="group_data-revid"><?php // print $item['revid']; ?></span>
            <span class="group_data-current"><?php // print $item['current']; ?></span>
            <span class="group_data-author"><?php // if ($item['author']) print $item['author']; ?></span>
            <span class="group_data-endorse"><?php // if ($item['endorse']) print $item['endorse']; ?></span>
            <span class="group_data-timestamp"><?php // print $item['timestamp']; ?></span>
          </div>
							<?php // } 
            // }  ?>
            -->
        </div>
        
        </fieldset>
      <?php print drupal_render($form['consensus_groups'][$id]); ?>
      </div> <!-- end of c_user_wrapper -->
    </li>
    <?php } ?>
  </ul>
  <?php /* print drupal_render($form['submit']);*/ ?>
  </form>
</div>