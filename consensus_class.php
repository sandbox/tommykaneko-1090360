<?php
/**
 * Consensus Module object class.
 *
 * The Consensus class is responsible for editing the XML in a structured way.
 *
 * Authored by Tom Kaneko <http://tomkaneko.com>
 * License: GNU GPL (version 2 or later). See LICENSE.txt for details.
 *
 * @author     Tom Kaneko <mail@tomkaneko.com>
 * @license    GNU GPL (version 2 or later). See LICENSE.txt for details.
 */

class consensus {
	
	// the simpleXML object representaion of the XML
	var $simpleXML;
	
	// the DOM object representation of the XML
	var $dom;
	
	// messages to return
	var $messages = array();
	
	// the user account performing the actions
	var $account;
	
	// timestamp used and stored on edits
	var $time;
			
	/**
	 * constructor creates $simpleXML array from xml string passed to constructor.  
	 * 
	 * If no string is passed, the function new_document($document) is called, which returns a string to save
	 *
	 * @param $node
	 * 		node object
	 * @param $new
	 * 		(optional) TRUE to populate a new Consensus XML document
	 *		FALSE to load the document from the $node body
	 * @param $account 
	 * 		(optional) User account with which to perform edits.  The global $user object will be used if there none is specified
	 * @param $time
	 *		(optional) Custom timestamp on which to perform edits.  The current time will be used if this variable is left out
	*/
	function __construct (&$node, $new = false, $account = NULL, $time = NULL) { 
		
		// set the time property
		if ($time != NULL) {
			$this->time = (int)$time;
		} else {
			$this->time = time();
		}

		// set the user who is editing
		if ($account) {
			$this->account = $account;
		} else {
			global $user;
			$this->account = $user;
		}

		// check to see if a node has been passed to the constructor, if not, create it
		if ($new == true) {
			if ($node == NULL) {
				echo '<br />consensus module: $document string is empty, cannot create new document';
			}
			else {
				// populate the $node with new document
				$node = $this->new_document($node);
			}
		}
				
		if (isset($node->xml)) {
			$simple = simplexml_load_string($node->xml);
 		} else {
			$simple = simplexml_load_string($node->body);
		}
		
		// set the formatting
		$dom = dom_import_simplexml($simple)->ownerDocument;		
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		

		if ($simple&&$dom) {
			$this->simpleXML = $simple;
			$this->dom = $dom;
		}
		else {
			drupal_set_message("consensus module: content is not valid XML");
			return false;
		}
	}
	
	/**
	 * Creates the basic Consensus XML document format.
	 *
	 * @param $node
	 * 		A $node object.  The $node->body property will be overwritten.
	 * @return
	 * 		An edited $node object with and XML
	 */
	function new_document (&$node) {
		
		$node->body = 
'<?xml version="1.0" encoding="UTF-8" ?>

<consensus>
  <issue>
    <id>'.$node->nid.'</id>
    <title>'.$node->title.'</title>
    <creation_time>'.$this->time.'</creation_time>
    <creator>
      <user>
        <id>'.$this->account->uid.'</id>
      </user>
    </creator>
  </issue>
  <content></content>
</consensus>
';

		return $node;	
	}
	
	/*
	*		Sanitizes strings, ready to input into XML document 
	*
	*		@param $string
	*			String to sanitize.
	*		@returns
	*			Sanitized string.
	*/
	function sanitize ($string) {
		$string = htmlspecialchars($string);
		return $string;
	}
	/**
	 *	Returns unique itemid in document.
	 *
	 *	@return
	 *		Unique Item ID integer
	 */
	function get_unique_itemid (){
		$items = $this->simpleXML->XPath('content//item');
		$item_count = count($items) + 1;
		while ($this->itemid_exists($item_count)) {
		  $item_count = $item_count +1;
		}
		return $item_count;
	}
	/**
	 *	Returns unique itemid in document.
	 *
	 *	@param $itemid
	 *		The item ID number to search for in the document.
	 *	@return
	 *		TRUE if item with $itemid exists.
	 *		FALSE if item with $itemid does not exist.
	 */
	function itemid_exists ($itemid) {
	  $items = $this->simpleXML->XPath('content//item[@id='.$itemid.']');
		if (empty($item[0])) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 *	Returns unique revision ID for given itemid.
	 *
	 *	@param $itemid
	 *		Item ID number to get unique revision number for  OR SimpleXMLElement object of item
	 *	@return
	 *		Unique revision item ID.
	 */
	function get_unique_revid ($itemid) {
		if (get_class($itemid) == 'SimpleXMLElement') {
			$revisions = $itemid->XPath('revision');
		} else {
			$item = $this->simpleXML->XPath('//item[@id="'.$itemid.'"]');
			$revisions = $item[0]->XPath('revision');
		}
		$revisions = count($revisions);
		
		$revid = $revisions + 1;
		return $revid;
	}
	
	/**
	 *	Returns a simpleXML Element of the specified item
	 */
	function get_item($itemid) {
		
		if ($itemid) {
			$item = $this->simpleXML->XPath('content//item[@id="'.$itemid.'"]');
		}
		
		return $item[0];
	}
	
	/**
	 *	Returns a simpleXML Element of the current revision within the specified item
	 */
	function get_current_revision ($itemid) {
		if (get_class($itemid) == 'SimpleXMLElement') {
			$item = $itemid;
		} else {
			$item = $this->simpleXML->XPath('//item[@id='.intval($itemid).']');
			$item = $item[0];
		}
		$revision = $item->XPath('revision[@current=1]');
		// return the reverted revision if this is a reference revision
		if ($revision[0]->attributes()->revertTo) {
			$revision = $item->XPath('revision[@revid='.$revision[0]->attributes()->revertTo.']');
		}
		return $revision[0];
	}

	/**
	 *	Returns a simpleXML Element of the specified revision.
	 *
	 *	@param $itemid
	 *		The itemid number to search for *OR* the SimpleXMLElement of the item
	 *	@param $revid
	 *		(optional) The revision id within the item.  If the parameter is not passed, the most current revision is returned.
	 */
	function get_revision ($itemid, $revid = NULL) {
	  if ($revid == NULL) {
		  return $this->get_current_revision($itemid);
		}
		else {
			if (get_class($itemid) == 'SimpleXMLElement') {
				$item = $itemid;
			} else {
				$item = $this->simpleXML->XPath('//item[@id='.intval($itemid).']');
				$item = $item[0];
			}
			$revision = $item->XPath('revision[@revid='.intval($revid).']');
			if ($revision[0]->attributes()->revertTo) {
				$revision = $this->get_revision($item, $revision[0]->attributes()->revertTo);
			}
			return $revision[0];
		}
	}
	
	/**
	 *	get the text content of the revision item
	 *	@param $revid
	 *		(optional) If specified, returns the text of specified revisionid.  Defaults to current revision.
	 */
	function get_revision_text ($itemid, $revid = NULL, $language = 'en') {
    $revision = $this->get_revision($itemid, $revid);
		
		$text = $revision->text;
		
		if ($revision->text->attributes()->language != $language) {
			$translation = $revision->XPath('translation[@language="'.$language.'"]');
			if (!empty($translation)) {
				$t_revision = $translation->XPath('revision[@current=1]');
				$text = $t_revision->text;
			}
		}
    return $text;
  }
	
	/**
	 *	get the text from the latest translation revision
	 */
	function get_translation_text ($itemid, $revid = NULL, $language = 'en') {
    $revision = $this->get_revision($itemid, $revid);
		$translation = $revision->XPath('translation[@language="'.check_plain($language).'"]');
    
		if ($translation) {
			$revision = $this->get_current_revision($translation);
			$text = $revision->text->asXML();
		}
		if ($text) {
			$search = array('<text>', '</text>');
    	return str_replace($search, '', $text);
		} else {
			return false;
		}
	}

	function get_authorid ($itemid, $revid = NULL) {
		$revision = $this->get_revision ($itemid, $revid);
		return $revision->author->user->id;
	}
	
	function get_revision_attributes ($itemid, $revid = NULL) {
		$revision = $this->get_revision ($itemid, $revid);
		$attributes = $revision->attributes();
		$new = array();
		foreach ($attributes as $key => $attribute) {
			$new[$key] = $attribute; 
		}
		return $new;
	}
	// returns simpleXML as XML string
	function return_xml () {
		$simple = &$this->simpleXML;
		$xml = $simple->asXML();
		
		return $xml;
	}
	
	// runs a DOM XPath query
	function dom_xpath ($query, $object = NULL) {
		
		if (!is_object($object)) {
			$xpath = new DOMXPath($this->$dom);
		} else {
			$xpath = new DOMXPath($object);
		}
		$parentnode = $xpath->query($query);
		
	}
	
	/**
	 *	Sets the default document language
	 */
	function set_language ($lang) {
		
		if ($language = $this->simpleXML->XPath('//issue/language')) {
			$language[0] = check_plain($language);
		} 
		// no language element, create a new one
		else {
			$issue = $this->simpleXML->XPath('//issue');
			$language = $issue[0]->addChild('language', check_plain($lang));
		}
		
	}
	
	// returns simpleXML of all users in the XML
	/*
	function get_unique_users () {
		$users = $this->simpleXML->XPath('content//user');
		$unique_users = new simpleXML;
	
		foreach ($users as $user) {
			if ($unique_users->XPath('user$user->id
		}
	}
	*/
	
	// adds <user> tag, which contains information oabout the user
	function insert_user (&$item) {
	
		if ($this->account->realname) {
			$this->account->name = $this->account->realname;
		}
		
		if ($this->account){
			$user = $item->addChild('user');
			$user->addChild('id', $this->account->uid);
			$user->addChild('name',check_plain($this->account->name));	
			
			return $user;
		}
		else {
			echo 'consensus module: user does not exist or is not logged in';
			exit;
		}
		
	
	}

	//adds <author> tag to specified simpleXML item  **new method 1**
	function insert_author (&$item) {
		
		$author = $item->addChild('author');
		$author->addAttribute('timestamp',$this->time);
		$this->insert_user($author); // load user tags
		
		return $author;
	}
	
	// adds <endorse> tag to specified simpleXML item
	function insert_endorse($approve = true, $revision = null, $param = array(), $reverted = true){
	
		// if no object is passed to the $item variable, we find it by using itemid and revid
		if (get_class($revision) != 'SimpleXMLElement') {
			if ($param['revid'] && $param['itemid']) {				
				$revision = $this->get_revision(intval($param['itemid']), intval($param['revid']));
			}
			else {
				drupal_set_message('insufficient parameters specified for insert_endorse', 'error');
				return false;
			}
		} else {
			$temp = $revision->XPath('parent::item');
			$item =	$temp[0];
		}
		/*
		// retract and approve endorsements from all copies of this revision (reverted revisions)
		if ($reverted) {
			// get originals of this revision
			$reverts1 = $revision->XPath('revert');
			$revisions = array();
			foreach ($reverts1 as $revert) {
				$rev = $item->xpath('revision[@revid='.$revert['fromrevid'].']');
				$revisions[] = $rev[0];
			}
			// get copies of this revision
			$reverts2 = $item->XPath('revert[@fromrevid='.$revision['revid'].']');
			foreach ($reverts2 as $revert) {
					$rev = $revert->xpath("parent::revision");
					$revisions[] = $rev[0];
			}
			if (count($revisions) > 0) {
				foreach ($revisions as $revrevision) {
					if ($revrevision['revid'] != $revision['revid']) {
						$this->insert_endorse($approve,  $revrevision, $param, false);
					}
				}
			}
		}
		*/
		
		// check to see if the user has already entered an endorsement
		foreach ($revision->xpath('endorse') as $endorse) {
			$userid = $endorse->user->id;
			if ($this->account->uid == $userid) {
				// check if the endorse is active
				if ($endorse->attributes()->active == 1) {
				
					if ($approve == true) {

						$action = 'endorse';
						// insert contextual information
						$this->insert_context($endorse, $action, $param);
						return $endorse;
					} else {
						// endorsement retracted, so disable current endorsement
						$endorse->attributes()->active = 0;
						$endorse->attributes()->timestamp = $this->time;
						// update the note
						if ($note) {
							$endorse->note = check_plain($param['note']);
						}
						$action = 'retract';
						// insert contextual information
						$this->insert_context($endorse, $action, $param);
						return $endorse;
					}
				} 
				// if endorse is not active, reactiveate it
				else {
					if ($approve == false) {
						$action = 'retract';
						// insert contextual information
						$this->insert_context($endorse, $action, $param);
						return $endorse;
					} else {
						// endorsement active, so update the endorse values
						$endorse->attributes()->active = 1;
						$endorse->attributes()->timestamp = $this->time;
						// update the note
						if ($param['note']) {
							$endorse->note = check_plain($param['note']);
						}
						$action = 'endorse';
						// insert contextual information
						$this->insert_context($endorse, $action, $param);
						return $endorse;
					}
				}
			}
		}
		
		if ($approve == true) {
			// add the tags into the item
			$endorse = $revision->addChild('endorse');
			$endorse->addAttribute('timestamp',$this->time);
			$endorse->addAttribute('active',1);
			$this->insert_user($endorse); // load user tags
			$endorse->addChild('note', check_plain($param['note']));
			
			$action = 'endorse';
			// insert contextual information
			
			$this->insert_context($endorse, $action, $param);
			
			return $endorse;
		} else {
			drupal_set_message(t('You have not approved this item'), 'error');
			return false;
		}
		
	}
	
	/**
	 *	Inserts a <context> tag with information about context
	 *	@param $item
	 *		SimpleXMLElement in which to insert context tag
	 */
	function insert_context (SimpleXMLElement $endorse, $action, $param = array()){ 
		
		// check that $param['parentid'] and $param['parentRevid'] are set
		if (!$param['parentid']) {
			$items = $endorse->XPath('ancestor::item');
			if ($items[1]) {
				$param['parentid'] = $items[1]->attributes()->id;
			} else {
				$param['parentid'] = 0;
			}
		}
		if (!$param['parentrevid']) {
			$items = $endorse->XPath('ancestor::item');
			if ($items[1]) {
				$revisions = $items[1]->XPath('revision');
				$revision = end($revisions);
				$param['parentrevid'] = $revision->attributes()->revid;
			} else {
				$param['parentrevid'] = 0;
			}
		}
		
		// check that specific context does not already exist
		$contexts = $endorse->XPath('context[@action="'.check_plain($action).'"]');
		
		$current = '';
		$match = false;
		foreach ($contexts as $context) {
			$attributes = $context->attributes();
			if ($attributes->parentId == $param['parentid'] &&
					$attributes->parentRevId == $param['parentrevid']) {
				$match = true;
				$current = $context;
				break;
			}
		}
		
		if ($match) {
			$current->attributes()->timestamp = $this->time;
		} else {
			// add new context tags
			$new = $endorse->addChild('context');
			$new->addAttribute('action', check_plain($action));
			$new->addAttribute('parentId', intval($param['parentid']));
			$new->addAttribute('parentRevId', intval($param['parentrevid']));
			$new->addAttribute('timestamp', $this->time);
		}
		
		return $endorse;
	}
	
	function append_simplexml($parent,  $new_child){
    $node1 = dom_import_simplexml($parent);
    $dom_sxe = dom_import_simplexml($new_child);
    $node2 = $node1->ownerDocument->importNode($dom_sxe, true);
    $node1->appendChild($node2);
	} 
		
	// adds <revision> for the specified itemid specified simpleXML item **new method 1**
	function insert_revision ($item = NULL, $param, $extra) {
		
		if (get_class($item) != 'SimpleXMLElement') {
			//get revid before a new revision is made
			$item = $this->simpleXML->XPath('content//item[@id="'.check_plain($param['itemid']).'"]');
			$item = $item[0];
		}
		
		// if itemid has been passed, then use function to find revid.  Otherwise, set it to 1.
		if ($item) {
			$revid = $this->get_unique_revid($item);
		} elseif($param['itemid']) {
			$revid = $this->get_unique_revid(intval($param['itemid']));
		} else {
			$revid = 1;
		}
				
		// check that $param['parentid'] and $param['parentRevid'] are set
		if (!$param['parentid']) {
			$items = $item->XPath('ancestor::item');
			if ($items[0]) {
				$param['parentid'] = $items[0]->attributes()->id;
			} else {
				$param['parentid'] = 0;
			}
		}
		if (!$param['parentrevid']) {
			$items = $item->XPath('ancestor::item');
			if ($items[0]) {
				$revisions = $items[0]->XPath('revision');
				$revision = end($revisions);
				$param['parentrevid'] = $revision->attributes()->revid;
			} else {
				$param['parentrevid'] = 0;
			}
		}

		// change the 'current' attribute of previos revision to 0
		$result = $item->XPath("revision[@current='1']");
		if ($result) {
			$result[0]['current'] = '0';
		}
				
		// add new revision
		$revision = $item->addChild('revision');
		$revision->addAttribute('revid',$revid);
		$revision->addAttribute('type',check_plain($param['type']));
		$revision->addAttribute('timestamp',$this->time);
		$revision->addAttribute('current','1'); // make this the current revision
		// insert the parent id
		//$parent = $revision->xpath("parent::item");
		if (is_object($parent) && $parent->attributes()->id) {
			//$parentid = $parent->attributes()->id;
			$revision->addAttribute("parentId",intval($param['parentid']));
			$revision->addAttribute("parentRevId",intval($param['parentrevid']));
		} else {
			$revision->addAttribute("parentId","");
			$revision->addAttribute("parentRevId","");
		}
		
		// add author tags into the revision tag
		$this->insert_author($revision);
		
		// if text is plain, just add it normally
		$text = $revision->addChild('text', check_plain($param['text']));
		$text->addAttribute('language', check_plain($param['language']));
		
		if ($param['note']) {
			$revision->addChild('note', check_plain($param['note']));
		}
		if ($param['reason']) {
			$revision->addChild('reason', check_plain($param['reason']));
		}
		
		// if there is extra data, insert it in the extra tag
		if (is_array($extra) && !empty($extra)) {
			$externalData = $revision->addChild('externalData');
			foreach ($extra as $name => $content) {
				if (is_string($content)) {
					// check that any XML is valid before adding data
					$node = '
<node>'.$content.'</node>
';
					if (simplexml_load_string($node)) {
						// input the data
						$externalData->addChild($name, $content);
					}
				}
			}
		}
		
		return $revision;		
	}
	
#	insert_revert(&$item, $revid, $itemid, $note)

	function insert_revert  ($item = NULL, $revert_to_id=NULL, $itemid=NULL, $note) {
		
		//get revid before a new revision is made
		if (get_class($item) != 'SimpleXMLElement') {
			$item = $this->simpleXML->XPath('content//item[@id="'.$itemid.'"]');
			$item = $item[0];
		}
		
		// if itemid has been passed, then use function to find revid.
		if ($itemid) {
			$revid = $this->get_unique_revid($itemid);
		}
				
		// change the 'current' attribute of previos revision to 0
		$result = $item->XPath("revision[@current='1']");
		if ($result) {
			$result[0]['current'] = '0';
		}
				
		// add new revision
		$revision = $item->addChild('revision');
		$revision->addAttribute('revid',intval($revid));
		$revision->addAttribute('timestamp',$this->time);
		$revision->addAttribute('current','1'); // make this the current revision

		if ($revert_to_id&&$itemid) {
			// add reference to the reverted To item
			$revision->addAttribute('revertTo',intval($revert_to_id));
		} else {
			drupal_set_message(t('insufficient parameters specified for reverting'), 'error');
			return false;
		}
		
		/*
		// once attributes are set, copy the contents into the new revision
		if ($revert_to_id&&$itemid) {
			$item = $this->simpleXML->XPath('//item[@id="'.$itemid.'"]');
			$item = $item[0];
			$oldrevision = $item->XPath('revision[@revid="'.$revert_to_id.'"]');
			$oldrevision = $oldrevision[0];
		}
		else {
			drupal_set_message(t('insufficient parameters specified for reverting'), 'error');
			return false;
		}
		// copy the type attribute from old revision
		$revision->addAttribute('type',$oldrevision->attributes()->type);
		
		// clone the node
		$oldrevision = simplexml_load_string($oldrevision->asXML());
		// append all children to the new revision
		foreach ($oldrevision as $child) {
			$this->append_simplexml($revision, $child);
		}
		// get a unique revert id
		$revertid = count($revision->XPath('revert')) + 1;
		
		*/
				
		$this->insert_author($revision); // load user tags
		if ($note) {
			$revision->addChild('note', check_plain($note));
		}
		
		return $revision;
	}
	
	
	//inserts item into the xml
	function insert_item ($param = array(), $extra = NULL) {
		
	
	//$text, $type, $parentid, $beforeid = NULL, $note, $reason, $parentrevid,  $extra = NULL) {
		
		$doc = &$this->simpleXML;
		$dom = &$this->dom;
		$itemid = $this->get_unique_itemid();
		$param['text'] = trim($param['text']);

		if (empty($param['text'])) {
			drupal_set_message('No text has been submitted', 'error');
			return false;
		}

		$xpath = new DOMXPath(&$dom);
		
		// set the parent node into which the item is inserted
		if ($param['parentid']) {
			$parentnode = $xpath->query('content//item[@id="'.check_plain($param['parentid']).'"]')->item(0);
		}
		else {
			$parentnode = $dom->getElementsByTagName('content')->item(0);
		}
		
		// create node item
		$new = $dom->createElement('item');
		
		if ($param['beforeid']) {
			// get node target, which we will append to
			$target = $xpath->query('//item[@id="'.check_plain($param['beforeid']).'"]');
			$target = $target->item(0);
			
			if (is_object($target)) {
				// insert new node before target
				$new = $target->parentNode->insertBefore($new, $target);
			} else {
				drupal_set_message (t('Invalid parameters were given - beforeid'), 'error');
				return false;
			}
		}
		else {
			$new = $parentnode->appendChild($new);
		}
				
		// set id attribute
		$new->setAttribute("id",$itemid);
		
		// get the new element using simpleXML 
		$item = $doc->xpath('//item[@id="'.$itemid.'"]');
		$item = $item[0];
		
		unset($param['itemid']);
		// now populate item with content
		$this->insert_revision($item, $param, $extra);
		
		// $text, $type, false, $parentid, $parentrevid, $note, $reason, $extra); //load revision tag
				
		return $item;
	}	
	
	/**
	 *	Inserts a translation into the revision item
	 */
	function insert_translation (SimpleXMLElement $revision, $param = array()) {
		
		if (get_class($revision) != 'SimpleXMLElement') {
			// get SimpleXML revision element
			$revision = $this->get_revision(intval($param['itemid']), intval($param['revid']));
		}
		// check we have necessary variables
		if (!$param['language']) {
			drupal_set_message(t('insufficient parameters given'), 'error');
			return false;
		}
		// check to see if translation in this language aready exist
		$translation = $revision->XPath('translation[@language="'.check_plain($param['language']).'"]');
		if (count($translation) == 0) {
			$translation = $revision->addChild('translation');
			$translation->addAttribute('language', check_plain($param['language']));
		} else {
			$translation = $translation[0];
		}
		$tRevision = $this->insert_revision($translation, $param);
		
		return $translation;
	}


	/**
	 *	Move items to specified locations
	 */
	function move_item ($itemid, $parentid = false, $beforeid = false, $note) {
		$doc = $this->simpleXML;
		$dom = $this->dom;
		
		$xpath = new DOMXPath($dom);

		//select item to move
		$element = $xpath->query("//item[@id='".$itemid."']");
		$element = $element->item(0);
		
		// set the id of old 'before' attribute
		$old_before = $element->nextSibling;
		if ($old_before) {
			$old_before = $old_before->getAttribute('id');
		}
		else {
			$old_before = 0;
		}
		
		
		// set the id of old 'parent' attribute
		$old_parent = $element->parentNode;
		if ($old_parent->tagName == 'content') {
			$old_parent = 0;
		}
		else {
			$old_parent = $old_parent->getAttribute('id');
		}

		//check that that it is in a different position
		if (intval($beforeid) == $old_before && intval($parentid) == $old_parent) {
			drupal_set_message (t('The item has not moved'), 'warning');
			return false;
		}
		// stop impossible move situations
		if ($itemid == $parentid) {
			drupal_set_message (t('The item has not moved'), 'warning');
			watchdog('consensus', 'possible user vandalism attempt when trying to move items',array(), WATCHDOG_WARNING);
			return false;
		}
		//select new parent
		if ($parentid) {
			$parent = $xpath->query('//item[@id="'.$parentid.'"]');
			$parent = $parent->item(0);
		}
		else {
			$parent = $element->parentNode;
		}
		
		if ($beforeid) {
			$before = $xpath->query('//item[@id="'.$beforeid.'"]');
			$before = $before->item(0);
			//insert before
			$before->parentNode->insertBefore($element, $before);			
		}
		else {
			$parent->appendChild($element);
		}
		
		//add revision item to node
		$item = $doc->xpath('item[@id="'.$itemid.'"]');
		$item = $item[0];		
		// function to be made up
		$this->insert_move ($item, $itemid, $parentid, $beforeid, $old_parent, $old_before, $note);
		return $item;
	}
	
	
	// inserts a <moved> tag to the item
	function insert_move (&$item, $itemid, $parentid, $beforeid, $old_parent, $old_before, $note) {
		
		// if no object is passed to the $item variable, we find it by using itemid and revid
		if (!$item) {
			if ($itemid) {
				$item = $this->simpleXML->XPath('content//item[@id="'.$itemid.'"]');
				$item = $item[0];		
			}
			else {
				echo 'insufficient parameters specified for insert_move';
				return false;
			}
		}
				
		// add the tags into the item
		$moved = $item->addChild('moved');
		$moved->addAttribute('timestamp',$this->time);
		$moved->addAttribute('fromBeforeId', intval($old_before));
		$moved->addAttribute('fromParentId', intval($old_parent));
		$moved->addAttribute('toBeforeId', intval($beforeid));
		$moved->addAttribute('toParentId', intval($parentid));

		$this->insert_user($moved); // load user tags
		if ($note) {
			$moved->addChild('note',check_plain($note));
		}
		
		return $moved;

	}
	
	/*
	// splits an item at the specified point
	function split_item($itemid = NULL, $character){
		if (!$itemid){ echo 'consensus module: no itemid passed to split_item'; exit;}
		
		// clone 2 items with same content as original item
		$new_item1 = $this->clone_item($itemid);
		
		$xpath = new DOMXpath($new_item1);
		foreach($xpath->query('//revision[@current=0]') as $node) {
		  $node->parentNode->removeChild($node);
		}
		
		$new_item2 = $this->clone_item($itemid);
		
		$xpath = new DOMXpath($new_item2);
		foreach($xpath->query('//revision[@current=0]') as $node) {
		  $node->parentNode->removeChild($node);
		}
		
		$new_item1->revision->text = consensus_filter_text(substr($new_item1->revision->text, 0, $character));
		$new_item2->revision->text = consensus_filter_text(substr_replace($new_item2->revision->text,'', 0, $character));
		
		return array ($new_item1, $new_item2);
	}
	*/
	
	
	// clones an item with new itemid and returns a simpleXML node
	function clone_item($itemid = NULL) {
		if (!$itemid){ echo 'consensus module: no itemid passed to clone_item'; exit;}
			
		$xpath = new DOMXPath($this->dom);
		$old_item = $xpath->query('//item[@id='.$itemid.']')->item(0);
		$new_item = $old_item->cloneNode(true);
				
		// insert the new node after old node
		if($old_item->nextSibling) {
        	$new_item = $old_item->parentNode->insertBefore($new_item, $old_item->nextSibling);
        } else {
        	$new_item = $old_item->parentNode->appendChild($new_item);
        }
		
		$new_itemid = $this->get_unique_itemid();
		$new_item->setAttribute('id', intval($new_itemid));
		
		$this->simpleXML->XPath('//item[@id='.$new_itemid.']');
		return $new_item[0];
	}
	
#	insert_revision ($text, $type, $position) - use insert_revision instead



#	insert_discussion ( ) **TODO
#	insert_revert ($revid, $itemid, $note)
#	insert_endorse ($itemid, $revid)
#	insert_hidden ($itemid, $note)
#	
#
#
#	


#	write_simplexml () - converts simpleXML array into XML string, then writes the output to node body.
	function write_simplexml (&$node) {
	
		$doc = &$this->simpleXML;
		$dom = &$this->dom;
		
		$this->dom->formatOutput = true;		
		//save to string and insert into node->body
		$xml = $dom->saveXML();
		$node->body = $xml;
		$node->teaser = $xml;
		
		// create new revision TODO: may not need the Drupal revision system at all.
		//if ($node->nid) {
		//	$node->revision = TRUE;
		//	$node->revision_uid = $user->uid;
		//}
		
		$node = node_submit($node);
		node_save($node);
		
		// check if nid is set in the XML, and insert it if does not
		if (empty($doc->issue->id)) {
			$doc->issue->id = intval($node->nid);
			$xml = $dom->saveXML();
			$node->body = $xml;
			$node->teaser = $xml;
			// write the node to the database
			node_save($node);
		}
		
		return $node;
	}
	

#	get_simplexml_asstring () - returns simpleXML proerty as an XML string
}