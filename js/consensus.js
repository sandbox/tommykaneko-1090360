/**
$Id$
Consensus module Javascript file
**/

/**
 *	Initiate the required functions
 *
 */

/*Drupal.behaviors.consensus.attachBehaviors = function(context) {
  context = context || document;
  if (Drupal.jsEnabled) {
    // Execute all of them.
    jQuery.each(Drupal.behaviors.consensus., function() {
      this(context);
    });
  }
};*/
//document.prototype.loaded = false;

Drupal.behaviors.consensus = function (context) {
	
	//check initialisation has not already taken place
	if ($('.c_loaded', context).length != 0) {
		return false;
	}
	// check consensus content exists
	if ($('.c_content').length == 0) {
		return false;
	}

	Drupal.behaviors.consensus.prototype.initiate(context);
	// initiate users
	if (Drupal.behaviors.consensus.prototype.initiateUsers) {
		Drupal.behaviors.consensus.prototype.initiateUsers();
	}
	// initiate groups
	if (Drupal.behaviors.consensus.prototype.initiateGroups) {
		Drupal.behaviors.consensus.prototype.initiateGroups();
	}
	// initiate toolbox
	if (Drupal.behaviors.consensus.prototype.initiateToolbox) {
		Drupal.behaviors.consensus.prototype.initiateToolbox();
	}
	// initiate time slider
	if (	Drupal.behaviors.consensus.prototype.timeSlider) {
		Drupal.behaviors.consensus.prototype.timeSlider();
	}
}

// Check user has permission to edit document
Drupal.behaviors.consensus.prototype.initiate = function (context) {
		
		/**
		 *	Nid of page
		 */
		var temp = $('.c_content').attr('id').split('c_n');
		Drupal.behaviors.consensus.prototype.nid = temp[1];
		Drupal.behaviors.consensus.prototype.setRevisionIds(context);
		if (Drupal.settings.consensus.edit_permission) {
			Drupal.behaviors.consensus.prototype.generateLowerButtons (context);
			Drupal.behaviors.consensus.prototype.generateButtons (context);
			Drupal.behaviors.consensus.prototype.generateAddButtons (null, context);
			Drupal.behaviors.consensus.prototype.generateAllEndorseButtons(context);
		}
		Drupal.behaviors.consensus.prototype.revisionRollback(null, context);

		Drupal.behaviors.consensus.prototype.showHide(context);	
		
		// the document is loaded
		$('.c_content', context).addClass('c_loaded');
	}

/**
 *	Replace the content of node body
 */
Drupal.behaviors.consensus.prototype.replaceContent = function (body) {
	$('.c_content').replaceWith(body);
	Drupal.behaviors.consensus.prototype.pageRefresh();
}

/*
	*	assigns classes used to indicate highlighting and editing of items
	*/
Drupal.behaviors.consensus.prototype.selected = function(clicked) {
		
		$('.c_active').removeClass('c_active');
		var revision;
		if(clicked.hasClass('revision')) {
			revision = clicked;
		}else {
			if (clicked.closest('.revision').length != 0) {
				revision = clicked.closest('.revision');
			} else {
				return false;
			}
		}
		if (revision.length!=0) {
			revision.addClass('c_active');
			revision.siblings('.revision').addClass('c_active');
			return $('.c_active');
		} else {
			return false;
		}
	}


	/*
	* Generates a list of endorse buttons.  The function adds a clickable endorse button for
	*	each item with the class "revision" on the page.
	* 
	* On clicking on the endorse button, an AJAX request is sent to the site to register the
	* endorsement from the user.  On successful AJAX submission, the endorsement status is updated
	*/
Drupal.behaviors.consensus.prototype.generateAllEndorseButtons = function(context) {

		// append a note at the top of the page
		var content = $('.c_content', context);
		if (content.find('.c_endorse_label').length == 0) {
			content.prepend('<div class="c_endorse_label">'+Drupal.t('Your Endorsement')+'</div>')
		}
		// generate endorse button for each revision item
		var revisions = Drupal.behaviors.consensus.prototype.revisionIds;
		for (i=0; i<revisions.length; i++) {
			Drupal.behaviors.consensus.prototype.generateEndorseButton(revisions[i], context);
		}
	}
	
	
Drupal.behaviors.consensus.prototype.generateEndorseButton = function(revisionId, context) {
		var active = '';
		var contextOut = 'contextOut';
		var text = Drupal.t('Click to endorse');
		var revision = $('#'+revisionId, context);
		if (revision.find('.c_endorse_button').length != 0) {
			return false;
		}
		var endorses = revision.find('.data > .data-endorse');
		
		// determine the parent items for context
		var parentitem = revision.closest('.consensus_item').parent().closest('.consensus_item');
		if (parentitem.length == 0) {
			var parentid = 0;
			var parentrevid = 0;
		} else {
			var parentid = parentitem.attr('id');
			parentid = parentid.split('item');
			parentid = parentid[1];
			
			var parentrevitem = parentitem.children('.revision:visible');
			if (parentrevitem.length == 0) {
				parentrevitem = parentitem.children('.revision.current');
			}
			var parentrevid = parentrevitem.attr('id').split('r');
			parentrevid = parentrevid[1];
		}

		// check through the endorsement information
		inner:
		for (f=0; f<endorses.length; f++) {
			var endorse = $(endorses[f]);
			if(endorse.attr('data-uid') == Drupal.settings.consensus.uid) {
				var conts = endorse.find('.data-context');
	
				active = 'active';
				contexter:
				for (g=0; g<conts.length; g++) {
					var cont = $(conts[g]);
					if (cont.attr('data-action') == 'endorse' &&
							cont.attr('data-parentid') == parentid &&
							cont.attr('data-parentrevid') == parentrevid) {
						contextOut = '';
						text = Drupal.t('Click to remove my endorsement');
						break inner;
					}
				}
				text = Drupal.t('Click to endorse in this context');
				break inner;
			}
		}
		revision.prepend('<a href="javascript:void(0);" class="c_endorse_button '+active+' '+contextOut+'" title="'+ text +'" name="endorse_'+revision.attr('id')+'"></a>');
}
	
Drupal.behaviors.consensus.prototype.endorseButtonsHandler = function (button) {
				// build the data
				var data = Object();
				var itemid = button.closest('.consensus_item').attr('id');
				var temp;
				
				// check to see if the context needs to be updated
				if (button.hasClass('contextOut')) {
					 var updateContext = true;
				}
				
				if (button.hasClass('active') && !updateContext) {
					var action = 'insert_retract';
					data.retract = 1;
				} else {
					var action = 'insert_endorse';
					data.endorse = 1;
				}
				
				// add context data to the data object
				temp = $('#'+itemid).parent().closest('.consensus_item').attr('id');
				if (temp) {
					temp = temp.split('item');
					data.parentid = temp[1];
					temp = $('#'+itemid).parent().closest('.consensus_item').children('.revision:visible').attr('id');
					temp = temp.split('r');
					data.parentrevid = temp[1];
				}
				
				temp = button.closest('.consensus_item').attr('id').split('item');
				data.itemid = temp[1];
				data.nid = Drupal.behaviors.consensus.prototype.nid;
				data.action = action;
				var revision = button.closest('.revision');
				temp = revision.attr('id').split('r');
				data.revid = temp[1];
				data.endorse = 1;
				data.note = 'casual';
				var dataString = $.toJSON(data);
				var url = Drupal.settings.basePath+'consensus/ajax/'+data.nid; // Drupal path needs to be defined

				button.css('display', '').append(Drupal.behaviors.consensus.prototype.loadingGif(true)); 

				jQuery.post( url, {data:dataString}, function(response){ 
					
					if (response) {
						Drupal.behaviors.consensus.prototype.loadingGif(false);
						button.css('display', '');
						if (action == 'insert_endorse') {
							button.addClass('active');
							button.closest('.consensus_item').removeClass('c_unendorsed');
						} else if (action == 'insert_retract') {
							button.removeClass('active');
							button.closest('.consensus_item').addClass('c_unendorsed');
						}
						
						// replace the HTMl contents
						var consensus_item = $(response);
						if (consensus_item) {
							var new_revision = consensus_item.children('#'+revision.attr('id'));
							revision.replaceWith(new_revision);
						
							var context = $('#'+revision.attr('id')).parent();
							Drupal.attachBehaviors(context);
						}
						
					}
				});
				return false;
	}
	
	/* 
	* The function shows / hides the editing panes on clicking on a revision item within 
	* the page.
	*
	* The function checks whethere there are any active edits before showing the panes.
	*/
Drupal.behaviors.consensus.prototype.showHide = function (context) {
		// prevent clicks from going to the anchors
		//$('a.c_bookmark').click(function(event) { 
		//	event.preventDefault();
    //	return false; 
		//}); 

	// if the context is not the document, return false
	if ($('.c_content', context).length == 0) {
		return false;
	}
		/*
		*	Hides the editing / status window of a revision item
		*
		* Generates a "Close Window" button for each edit / details pane
		*/
	var closeWindow  = function (event) {
		if($(event.target).parents().index($('.c_content')) == -1 || $(event.target).parents('.c_close_window').length != 0) {
				$('.c_active').removeClass('c_active');
				// edit panes
				$('.revision:visible:not(this) .slide_wrapper:visible, .slide_wrapper.lower').slideUp(400);
				// add panes
				Drupal.behaviors.consensus.prototype.generateAddButtons(false);
				//revisionRollback(false);
				//event.stopPropagation(console.log('stopProp'));
		}

	};

	$(document, context).click(function(e) { 
			 var clicked = $(e.target);
			if (clicked.closest('ul.c_edit_buttons').length != 0) {
				Drupal.behaviors.consensus.prototype.editButton(clicked);
			}
			else if (clicked.closest('.c_edited').length != 0) {
			}
			
			else if (clicked.closest('.c_close_window a').length != 0) {
				closeWindow(e);
			}
			
			else if (clicked.closest('a.c_revision_back').length != 0) {
			// bind click events to changing the revision
				var revision_target = clicked.closest('.revision');
				var prev = revision_target.prevAll('.revision');
				prev = $(prev[0]);
				if (prev.length !=0) {
					revision_target.fadeOut('fast', function() {
						revision_target.removeClass('top');
						prev.fadeIn('fast', function() {
							prev.addClass('top').css('display', '');
						});
					});
				}
			}
			
			else if (clicked.closest('a.c_revision_forward').length != 0) {
			
				var revision_target = clicked.closest('.revision');
				var next = revision_target.nextAll('.revision');
				next = $(next[0]);
				if (next.length !=0) {
					revision_target.fadeOut('fast', function() {
						revision_target.removeClass('top');
						next.fadeIn('fast', function() {
							next.addClass('top').css('display','');
						});
					});
				}
			}
			
			else if (clicked.closest('a.c_revision_save').length != 0) {
				clicked = clicked.closest('a.c_revision_save');
				Drupal.behaviors.consensus.prototype.editButton(clicked);
			}
			else if (clicked.closest('ul.c_add_buttons').length != 0) {
				clicked = clicked.closest('a');
				Drupal.behaviors.consensus.prototype.addButton.call(clicked);
			}
			
			else if (clicked.closest('a.c_endorse_button').length != 0) {
				var button = clicked.closest('a.c_endorse_button');
				Drupal.behaviors.consensus.prototype.endorseButtonsHandler(button);
				e.preventDefault();
				return false;
			}

		// show editing panes if it not already shown
			else if (clicked.closest('.revision').length != 0) {
				
				var revision = clicked.closest('.revision');
				if (!revision.hasClass('c_active')){
					if (Drupal.behaviors.consensus.prototype.cancelEdit()) {
						Drupal.behaviors.consensus.prototype.selected(revision);
						/*
						if (Drupal.behaviors.consensus.prototype.initiateUsers) {
							// scroll to author
							// the following code is not compatible with all themes
							var author = revision.find('.data .data-author').attr('data-uid');
							var c = $('html').scrollTop();
							var d = $('#sidebar-right').scrollTop(); // 100 provides buffer in viewport
							var x = $('#uid-'+author).addClass('c_active_user').offset().top; // 100 provides buffer in viewport
							var z = x-c+d - 50;
						}
						*/
						/*
						$('#sidebar-right').animate({scrollTop: z}, 500);
						*/
						
						// edit panes
						//$('.revision:visible:not(this) .slide_wrapper:visible').add($('.slide_wrapper.lower:visible')).slideToggle(400);
	
						//$(".c_active .slide_wrapper.lower").css({'position':'absolute','visibility':'hidden','display':'block'});
						//var lowerHeight = $('.c_active .slide_wrapper.lower').outerHeight();
						//$(".c_active .slide_wrapper.lower").css({'position':'','visibility':'','display':''});
						
						// set the width of the absolutely positioned element
						var current_width = revision.outerWidth() - 20;
						// set the offset of the item
						var offset = revision.outerHeight();
						$('.c_active .slide_wrapper.upper').css({'width': current_width+'px', 'bottom': offset+'px'});
	
						
						$('.c_active .slide_wrapper').add($('.c_active:visible').next('.slide_wrapper.lower')).slideToggle(400, function () {
							//$(this).siblings('.revision').children('.slide_wrapper');
						});
	
						if (Drupal.settings.consensus.edit_permission) {
							// add panes
							Drupal.behaviors.consensus.prototype.generateAddButtons(revision, context);
						}
						/*
						// extra pane
						$('.revision:visible:not(this) .c_extra:visible').slideToggle(400);
						$('.c_active .c_extra').slideToggle(400, function () {
							//$(this).siblings('.revision').children('.c_extra').css('display', 'block');
							$(this).siblings('.revision').addClass('c_active');
						});
						*/
						// revision rollback pane
						Drupal.behaviors.consensus.prototype.revisionRollback(clicked, context);
					}
				}
			} else {
				if (Drupal.behaviors.consensus.prototype.cancelEdit()) {
					closeWindow(e);
				}
			}
	});


	
}

	/*
	*	Inserts the revision rollback buttons for every item on the page.
	*
	* The function inserts the revision rollback window within the details pane.  Each button
	* within the revision pane is bound to an event which will allow the user to scroll
	* through the different revisions, and revert to a revision if necessary.
	*/
	Drupal.behaviors.consensus.prototype.revisionRollback = function (target, context) {
		// if target is null, initiate the revision scrolling
		if (target === null) {
			//var currentid = target.attr('id');
			//var displayid;
			
			//var extra = target.children('.c_extra').append('<div class="c_revision_rollback"></div>');
			
			// add the rollback pane to each revision
			var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
			for (i=0; i<revisionIds.length; i++) {
				var revision = $('#'+revisionIds[i], context);
				var extra = revision.find('.c_extra');
				var html ='';
				
				if (extra.find('.c_revision_rollback').length == 0) {
					//extra.append('<div class="c_revision_rollback"></div>');
					html = '<div class="c_revision_rollback">';
					// if there are other revisions append forward and/or back links
					if (revision.siblings('.revision').length > 0) {
						var revno = revision.prevAll('.revision').length + 1;
						var totalrev = revision.siblings('.revision').length + 1;
						html += '<span>'+revno+Drupal.t(' of ')+totalrev+Drupal.t(' revisions')+'</span>'
						
						// add save button if this revision is not the current one
						if (!revision.hasClass('current') && 	Drupal.settings.consensus.edit_permission) {
							html += '<a class="c_revision_save" name="insert_revert"><span>'+Drupal.t('save')+'</span></a>'
						}
						if (revision.prevAll('.revision').length !=0) {
							//var revid = revision.prev('.revision').attr('id');
							html += '<a class="c_revision_back"><span>'+Drupal.t('back')+'</span></a>';
							}
						if (revision.nextAll('.revision').length !=0) {
							//var revid = revision.prev('.revision').attr('id');
							html += '<a class="c_revision_forward"><span>'+Drupal.t('forward')+'</span></a>';
						}
						extra.children('.c_revision_rollback').append(html);
					} 
					// no other revisions
					else {
						html += '<span class="c_no_revisions">'+Drupal.t('No other revisions')+'</span>';
					}
					html += '</div>';
					extra.append(html);
				} else {
				}
			}
			
			//$('a.c_revision_save').click(Drupal.behaviors.consensus.prototype.editButton());

		} 
		// if target is false, remove the Rollback pane
		else if (target == false) {
			$('.c_revision_rollback', context).slideToggle(400, function (event) {
				 $(this).replaceWith('');
			 });
		} 
		// if the target is a revision item
		else {
			if (!target.hasClass('revision')) {
				target = target.closest('.revision');
			}
			if (target.length != 0) {		
			var c_item = target.closest('consensus_item');
			}
		}
	}
	
	// generates add item buttons around the target item 
	// @param - target - the target item to append add buttons around.  if set to null, the document is assumed to be empty
Drupal.behaviors.consensus.prototype.generateAddButtons = function (target, context) {
		// hide visible add panes
		$('div.c_add_pane:visible', context).slideToggle(200, function() {
			$(this).replaceWith('');
		});
		
	
		if (target == null) {
			// is the document empty, or is the page initiating?
			if ($('.c_content').find('.consensus_item').length == 0) {
				// assume that the document is empty
				var setItem = Drupal.settings.consensus.default_options.content.add;
				for (var itemType in setItem) {
					if (setItem[itemType].enabled){
					add_list += '<li class="c_add_'+ setItem[itemType].action + '_' + itemType + '"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
					}
				}
				
				$('.c_content').append('<div class="c_add_pane" style="display:block;"><ul class="c_add_buttons">'+ add_list +'</ul></div>');
			}
		} else if (target == false){
			$('div.c_add_pane', context).slideToggle(200, function() {
				$(this).replaceWith('');
			});
		} else {
			var add_list = '';
			// put the target on to the consensus item, and not the revisions of it
			if (!target.hasClass('consensus_item')) {
				target = target.closest('.consensus_item');
			}
			var parent = target.parent().closest('.consensus_item');
			var parent_type = '';
			
			if (parent.hasClass('consensus_item')) {
				parent_type = parent.children('.revision:visible').find('.data > .data-type').text();
			} else {
				parent_type = 'content';
			}
			if (parent_type == 'content') {
				var setItem = Drupal.settings.consensus.default_options[parent_type].add;
			} else {
				var setItem = Drupal.settings.consensus.item_options[parent_type].add;
			}
			for (var itemType in setItem) {
				if (setItem[itemType].enabled){
				add_list += '<li class="c_add_'+ setItem[itemType].action + '_' + itemType + '"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
				}
			}

			if (add_list != '') {
				//target.before('<div class="c_add_pane c_add_pane_prev" style="display:none;"><ul class="c_add_buttons">'+ add_list +'</ul></div>');
				target.after('<div class="c_add_pane c_add_pane_next" style="display:none;"><ul class="c_add_buttons">'+ add_list +'</ul></div>');
				$('.c_add_pane').slideToggle(200);
			}
		
		} 
		
	}
	
	// click event for add buttons
Drupal.behaviors.consensus.prototype.addButton = function() {
		if (Drupal.behaviors.consensus.prototype.cancelEdit()) {
			
			var action = $(this).attr('name');  // name of the action to be performed
			var type = $(this).attr('data-type');  // type of item
			var parentid = '';  //  the parentid on which to perform the action
			var temp = new Array();  // temporary variable
			var obj = new Object();
			var nid = Drupal.behaviors.consensus.prototype.nid;
			
			$('.c_add_pane').css('display','none');
			
			switch (true){
			case (action=='insert_item'):
				
				if ($(this).closest('.consensus_item').length ==0) {
					parentid = $(this).closest('.c_content').attr('id');
				}else{
					parentid = $(this).closest('.consensus_item').attr('id');
				}
				if($(this).closest('.c_add_pane').next('.consensus_item').length == 0) {
					beforeId = false;
				}else{
					beforeId = $(this).closest('.c_add_pane').next().attr('id');
				}
				Drupal.behaviors.consensus.prototype.newItem(action, type, parentid, beforeId);
				break;
			}
		}
	}

Drupal.behaviors.consensus.prototype.generateButtons = function (context) {
		// build the editing pane for each revision item
		var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
		for (i=0; i<revisionIds.length; i++) {
			var edit_list = '';
			var edit_list_upper = '';
			var revision = $('#'+revisionIds[i], context);
			// check that buttons haven't been inserted already
			if (revision.find('.c_edit_pane').length != 0) {
				continue;
			}
				var type = revision.find('.data > .data-type').text();
			var setItem = Drupal.settings.consensus.item_options[type] ? Drupal.settings.consensus.item_options[type].edit: null;
			var endorsed = false;
			var endorses = revision.find('.data > .data-endorse');
			
			inner:
			for (f=0; f<endorses.length; f++) {
				if($(endorses[f]).attr('data-uid') == Drupal.settings.consensus.uid) {
					endorsed = true;
					break inner;
				}
			}
			
			// is the user the origianl author of this item?
			var authorId = revision.find('.data > .data-author').attr('data-uid');
			var owner = false;
			if (authorId == Drupal.settings.consensus.uid) {
				owner = true;
			}

			for (var itemType in setItem) {
				// put items in the upper edit box
				if (setItem[itemType].enabled && setItem[itemType].position == 'upper'){
					// if it is a revise (edit) button, check that the user has permission to edit it
					if (itemType == 'revise') {
						if (owner || Drupal.settings.consensus.item_options[type]['edit_permissions']['foreign']) {
							edit_list_upper += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
						}
					} 
					// if the item is endorse or retract, make sure that we get the right one
					else if (itemType == 'endorse' || itemType =='retract') {
						if (endorsed) {
							if (itemType == 'retract') {
						edit_list_upper += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
							}
						} else {
							if (itemType == 'endorse') {
						edit_list_upper += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
							}
						}
					}
					else {
						edit_list_upper += '<li class="c_edit_'+ setItem[itemType].action + '_' + itemType + '"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
					}
				} 
			}
			
			if (edit_list_upper.length != 0) {
				revision.find('.slide_wrapper.upper').append('<div class="c_edit_pane"><ul class="c_edit_buttons">'+ edit_list_upper +'</ul></div>');
			}
		}
		
		// create a close window box in each slide window
		var uWrappers = $('.slide_wrapper.upper');
		for (i=0; i<uWrappers.length; i++) {
			$(uWrappers[i]).append('<div class="c_close_window"><a>'+ Drupal.t('Close') +'</a></div>');
		}

	}

Drupal.behaviors.consensus.prototype.generateLowerButtons = function (context) {
		// build the editing pane for each revision item
		var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
		for (i=0; i<revisionIds.length; i++) {
			var revision = $('#'+revisionIds[i], context);
			// check that the LowerButtons have not been inserted already
			if(revision.find('.slide_wrapper.lower').length != 0) {
				continue;
			}
			
			var edit_list = '';
			var type = revision.find('.data > .data-type').text();
			var setItem = Drupal.settings.consensus.item_options[type] ? Drupal.settings.consensus.item_options[type].edit : null;

			var endorsed = false;
			var endorses = revision.find('.data > .data-endorse');
			
			inner:
			for (f=0; f<endorses.length; f++) {
				if($(endorses[f]).attr('data-uid') == Drupal.settings.consensus.uid) {
					endorsed = true;
					break inner;
				}
			}
			
			// is the user the origianl author of this item?
			var authorId = revision.find('.data > .data-author').attr('data-uid');
			var owner = false;
			if (authorId == Drupal.settings.consensus.uid) {
				owner = true;
			}
			
			for (var itemType in setItem) {
				// put the items in the lower edit box
				if (setItem[itemType].enabled && setItem[itemType].position != 'upper'){
					// if it is a revise (edit) button, check that the user has permission to edit it
					if (itemType == 'revise') {
						if (owner || Drupal.settings.consensus.item_options[type]['edit_permissions']['foreign']) {
							edit_list += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
						}
					} 
					// if the item is endorse or retract, make sure that we get the right one
					else if (itemType == 'endorse' || itemType =='retract') {
						if (endorsed) {
							if (itemType == 'retract') {
						edit_list += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
							}
						} else {
							if (itemType == 'endorse') {
						edit_list += '<li class="c_edit_'+ setItem[itemType].action +'"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
							}
						}
					}
					else {
						edit_list += '<li class="c_edit_'+ setItem[itemType].action + '_' + itemType + '"><a name="' + setItem[itemType].action + '" data-type="'+ itemType +'"><span>' + setItem[itemType].name + '</span></a></li>';
					}
				}
			}
			if (edit_list.length != 0) {
				revision.after('<div class="slide_wrapper lower"><div class="c_edit_pane"><span class="label">'+Drupal.t('Insert: ')+'</span><ul class="c_edit_buttons">'+ edit_list +'</ul></div></div>');
			}
		}
	}
	// handles click events for buttons
Drupal.behaviors.consensus.prototype.editButton = function (clicked) {
		if (Drupal.behaviors.consensus.prototype.cancelEdit()) {
			clicked = $(clicked).closest('a');
			var action = clicked.attr('name');  // name of the action to be performed
			var type = clicked.attr('data-type');  // type of item
			var itemid = '';  //  the itemid on which to perform the action
			var temp = new Array();  // temporary variable
			var obj = new Object();
			var nid = Drupal.behaviors.consensus.prototype.nid;
			if (clicked.closest('.revision').length == 0) {
				var target = clicked.closest('.slide_wrapper').prev('.revision');
			} else {
				var target = clicked.closest('.revision');
			}
			
			switch (true){
			case (action =='insert_revision'):
				var oldText = target.children('.data-text').text();
				itemid = target.parent().attr('id');
				if(target.closest('.consensus_item').nextAll('.consensus_item:first').length == 0) {
					beforeId = false;
				}else{
					beforeId = target.closest('.consensus_item').nextAll('.consensus_item:first').attr('id');
				}
				Drupal.behaviors.consensus.prototype.newRevision(action, itemid, oldText, beforeId);
				
				break;
			case (action=='insert_item'):
				if (target.closest('.consensus_item').length ==0) {
					parentid = target.closest('.c_content').attr('id');
				}else{
					parentid = target.closest('.consensus_item').attr('id');
				}
				
				if(target.nextAll('ul').children('.consensus_item:first').length == 0) {
					beforeId = false;
				}else{
					beforeId = target.nextAll('ul').children('.consensus_item:first').attr('id');
				}
				Drupal.behaviors.consensus.prototype.newItem(action, type, parentid, beforeId);
				break;
			case (action=='insert_retract'):
			case (action=='insert_endorse'):
				itemid = target.parent().attr('id');
				// get the revision id by manipulating the id attribute of div.revision
				temp = target.attr('id').split('r');
				revid = temp[1];
				
				Drupal.behaviors.consensus.prototype.newEndorse(action, itemid, revid);
				break;
			case (action == 'insert_move'):
				if (target.closest('.consensus_item').parent().hasClass('consensus_item')) {
					var parentid = target.closest('.consensus_item').parent().attr('id');
				} else {
					var parentid ='';
				}
				itemid = target.closest('.consensus_item').attr('id');
				if(target.closest('.consensus_item').nextAll('.consensus_item:first').length == 0) {
					beforeid = false;
				}else{
					beforeid = target.closest('.consensus_item').nextAll('.consensus_item:first').attr('id');
				}				
				Drupal.behaviors.consensus.prototype.newMove(action, itemid, beforeid, parentid);
				
				break;
			case (action =='insert_translation'):
				var oldText = target.children('.data-text').text();
				itemid = target.parent().attr('id');
				if(target.closest('.consensus_item').nextAll('.consensus_item:first').length == 0) {
					beforeId = false;
				}else{
					beforeId = target.closest('.consensus_item').nextAll('.consensus_item:first').attr('id');
				}
				Drupal.behaviors.consensus.prototype.newTranslation(action, itemid, oldText, beforeId);
				
				break;
			case (action == 'insert_revert'):
				var revid = target.attr('id');
				itemid = target.closest('.consensus_item').attr('id');
				var oldText = target.children('.data-text').text();
				
				Drupal.behaviors.consensus.prototype.newRevision(action, itemid, oldText);
				break;
			}
		}
	}


Drupal.behaviors.consensus.prototype.newMove = function (action, itemid, beforeid, parentid) {
		var temp;
		//$('.consensus_item').addClass('c_moving');
		var parent;
		var data = Object();
		
		
		// add panes
		$('.c_add_pane').slideToggle(function(){ $(this).remove();});

		//parent.addClass('sorting_parent');
		temp = itemid.split('item');
		data.itemid = temp[1];
		
		// populate the obj object ready for posting
		if (beforeid != '') {
			temp = beforeid.split('item');
			data.old_beforeid = temp[1];
		}
		else {
			data.old_beforeid = '';
		}
		if (parentid != '') {
			temp = parentid.split('item');
			data.old_parentid = temp[1];
		}
		else {
			data.old_parentid = '';
		}
		//sortHTML = $('#'+parentid);
		// check move settings for this type
		var type =  $('#'+itemid+' > .revision:visible').find('.data .data-type').text();
		if (Drupal.settings.consensus['item_options'][type]['move']['movable']) {
			// set the parent items
			switch (true) {
				// the item can only be moved within its current parent item
				case (Drupal.settings.consensus['item_options'][type]['move']['range'] == 'parent'):
					parent = $('#'+itemid).parent();
					parent.children('.consensus_item:visible').addClass('sorting');
					break;
				// the item can be moved to any part of the page
				case (Drupal.settings.consensus['item_options'][type]['move']['range'] == 'free'):
					parent = $('#'+itemid).closest('.c_content').children('ul');
					parent.find('.consensus_item:visible').addClass('sorting');
					var allowed_parent_types = Drupal.settings.consensus['item_options'][type]['move']['allowed_parent_types'];
					// only allow item to be nested in allowed parents
					$('.consensus_item').addClass('no-nest');
					for (var key in allowed_parent_types) {
						$('.consensus_item.c_' + allowed_parent_types[key]).removeClass('no-nest');
					}
					//$('.no-nest').removeClass('sorting');
				break;
			}
		} else {
			alert('this item cannot be moved');
			return false;
		}
		parent.addClass('sort_parent');
		// set the sortable items
		$('#'+itemid).addClass('c_moving').attr('parent', parentid).attr('before', beforeid).children('.revision:visible').css({border: "none",}).animate({paddingLeft: "3em",minHeight: "40px",}, 500, 'swing', 
			function(){ 
				$(this).addClass('c_moving_button'); 

				parent.nestedSortable({ 
				items: '.sorting',
				//items: 'li',
				disableNesting: 'no-nest',
				//axis: 'y',
				//containment: '.c_content',
				forceHelperSize: true, // may not be necessary
				listType: 'ul',	
				opacity: 0.8,
				//tabSize: 30,
				forcePlaceholderSize: true,
				//helper: 'clone', // if the display is better with clone
				//handle: 'div.c_moving_button',
				handle: 'div.revision',
				placeholder: 'c_placeholder',
				revert: true,
				tolerance: 'pointer',
				toleranceElement: '> div.revision',
				
				sort: function(event, ui) {
					$('.c_placeholder').css('height', $('.c_moving').outerHeight());
				},
				update:  function(event, ui) {					
						// populate the data object
						// get id of item immediately after the moved item
						if ($('#'+itemid).next('.consensus_item').length != 0) {
							temp = $('#'+itemid).next('.consensus_item').attr('id').split('item');									
							data.beforeid = temp[1];
						} else {
							data.beforeid = '';
						}
						
						if ($('#'+itemid).parent().closest('.consensus_item').length != 0) {
							temp = $('#'+itemid).parent().closest('.consensus_item').attr('id').split('item');
							data.parentid = temp[1];
						} else {
							data.parentid = '';
						}
						data.nid = Drupal.behaviors.consensus.prototype.nid;
						data.action = action;
						var url = Drupal.settings.basePath+'consensus/form/'+data.nid; // Drupal path needs to be defined
						var dataString = $.toJSON(data);
						jQuery.post( url, {data:dataString}, function(data){ 
							// insert the form
							$('#'+itemid).children('.revision:visible').append(data);
							parent.nestedSortable( "disable" );
							Drupal.behaviors.consensus.prototype.generateAddButtons(false);
							$('#'+itemid).children('.revision:visible').removeClass('c_moving_button').css({border:'', minHeight:''});
							//$('.c_move_hidden:not(.slide_wrapper)').removeClass('c_move_hidden').slideToggle(1000);
							
							//$('.c_moving').removeClass('c_moving');
							//$('.sorting').removeClass('sorting');
							//$('.c_moving_button').removeClass('c_moving_button');
						});
					}
			});
		});
		
	}
	
Drupal.behaviors.consensus.prototype.loadingGif = function (op) {
		if (op == null) {
			op = true;
		}
		if (op == true) {
			return '<img class="c_loading" src="'+Drupal.settings.basePath + Drupal.settings.consensus.module_path+'/css/images/loading.gif"/>';
		}
		if (op == false) {
			$('.c_loading').replaceWith('');
		}
	}
	
	// Checks to see if edit boxes are already in the page. 
	// If so, it cancels the editing boxes.
Drupal.behaviors.consensus.prototype.cancelEdit = function(askPrompt) {

		var r;
		if (askPrompt == null) {
			var askPrompt = true;
		}

		// check forms are not filled with new content
		if ($('#consensus-page-form').length != 0) {
			if ($('#consensus-page-form').siblings('.data-text').length) { // check to see if the textarea contents come from .data-text
				if ($.trim($('#consensus-page-form #edit-edit-area-text').val()) == $.trim($('#consensus-page-form .c_textarea').html())) {
					askPrompt = false;
				}
			}

			if (askPrompt) {
				if ($.trim($('#consensus-page-form textarea').val()) != '') { // check if text boxes are not empty
					r = confirm(Drupal.t('You will lose the changes you have made.  Are you sure you want to cancel editing?'));
				}
			}
			else {
				r = true;	
			}
			
			if ($.trim($('#consensus-page-form textarea').val()) == '' || r == true) { // check if text boxes are empty, or the user has confirmed that
				if ($('#consensus-page-form').siblings('.data-text').length) {
					$('#consensus-page-form').siblings('.data-text').css('display', 'block');
				}
				$('.c_edited').replaceWith('');
				$('#consensus-page-form').replaceWith('');
			}
			else {
				return false;
			}	
		}
		// clear hide box without prompt
		if ($('.c_select_box').length) {
			$('.c_select_box').replaceWith('');
			$('.current').children('.data-text').css('display','block'); // this will screw things up TODO
		}
		
		// remove highlight
		$('.c_active_user').removeClass('c_active_user');
		
		// cancel the sorting
		if ($( ".sort_parent" ).length != 0) {
			var disabled = $( ".sort_parent" ).nestedSortable( "option", "disabled" );
			if (disabled) {
				var oldparent = $('.c_moving').attr('parent');
				var oldbefore = $('.c_moving').attr('before');
				if (oldparent.length == 0) {
					oldparent = $('.c_content');
				} else {
					oldparent = $('#'+oldparent);
				}
				
				if (oldbefore.length == 0) {
					$('.c_moving').appendTo(oldparent);
				} else {
					$('.c_moving').insertBefore('#'+oldbefore)	;
				}
				
				// reset sortables
				$( ".sort_parent" ).nestedSortable("cancel").nestedSortable("destroy");	
			}else {
				$( ".sort_parent" ).nestedSortable("destroy");
			}
			$('.c_move_hidden').slideToggle(1000, function() {$('.c_move_hidden').css('display', 'block').removeClass('c_move_hidden');});
			$('.c_moving').children('.revision').css({'padding-left': '', 'min-height': '', 'border': ''});
			$('.c_moving, .sorting, .no-nest, .c_moving_button').removeClass('c_moving sorting no-nest c_moving_button'); // remove moving classes
			$('.sort_parent').removeClass('sort_parent');
		}
		// hide edit panes
		$('.slide_wrapper:visible').slideToggle(400, function () {});
		
		// detach any editors
		if (Drupal.wysiwygDetach && $('#consensus-page-form').length !=0 ) {
			Drupal.wysiwygDetach(document);
		} 
		
		return true;
	};

	
	// function for creating a textbox to submit revisions
	// @param action - name of the action to be performed
	// @param itemid - the itemid on which to perform the action
	// @param oldtext - the content of the text to be overwritten
Drupal.behaviors.consensus.prototype.newRevision = function(action, itemid, oldText, beforeid){
			
			var revision = $('#'+itemid).children('.revision:visible');
			if (revision.children('textarea.c_textarea').length == 0) {				
				
				var temp = Array();
		
				// get the form from server
				var data = Object();
				temp = itemid.split('item');
				data.itemid = temp[1];
				temp = temp[0].split('c_n');
				data.nid = Drupal.behaviors.consensus.prototype.nid;
				temp = $('#'+itemid).parent().closest('.consensus_item').attr('id');
				if (temp) {
					temp = temp.split('item');
					data.parentid = temp[1];
					temp = $('#'+itemid).parent().closest('.consensus_item').children('.revision:visible').attr('id');
					temp = temp.split('r');
					data.parentrevid = temp[1];
				}
				data.action = action;
				data.revid = revision.attr('id').replace('i'+data.itemid+'r', '');
				data.oldtext = oldText;
				if (beforeid) {
					temp = beforeid.split('item');
					data.beforeid = temp[1];
				}
				data.type = revision.find('.data-type').text();
				var dataString = $.toJSON(data);
				var url = Drupal.settings.basePath+'consensus/form/'+data.nid; // Drupal path needs to be defined
				//$('.slide_wrapper:visible').slideToggle(500);
				revision.append(Drupal.behaviors.consensus.prototype.loadingGif(true));
				jQuery.post( url, {data:dataString}, function(data){ 
					Drupal.behaviors.consensus.prototype.generateAddButtons(false);
					// insert the form
					Drupal.behaviors.consensus.prototype.loadingGif(false);
					revision.children('.data-text').css('display', 'none');
					revision.append(data);
					
					if (Drupal.behaviors.attachWysiwyg) {
						Drupal.behaviors.attachWysiwyg($('#consensus-page-form'));
					} else {
			
						$('.c_textarea, #edit-note').autoResize({
							
							animate : true,
							// On resize:
							onResize : function() {
								$(this).css({opacity:1});
							},
							// After resize:
							animateCallback : function() {
								$(this).css({opacity:1});
							},
							// Quite slow animation:
							animateDuration : 200,
							// More extra space:
							extraSpace : 30
						}); 
					}
					// simulate a keydown to resize the textarea to fit its contents
					$('.c_textarea').keydown();
	
		  }); // end tag of resize callback
			}
	};
	
	// create new item edit box
	// @param - action - string with the name of action eg 'insert_item'
	// @param - parentid - string with id of parent item in the form 'c_n'+nid+'item'+itemid eg. 'c_n1item2'
	// @param - beforeid - string with id of item immediately after this item in the form 'c_n'+nid+'item'+itemid eg. 'c_n1item2'
Drupal.behaviors.consensus.prototype.newItem = function (action, type, parentid, beforeid) {
		var parent_item = '';
		var temp = Array();
		
		if (parentid) {
			parent_item = $('#'+parentid);
		}
		else {
			alert('error with newItem - not sufficient parameters');
		}

		// assign class names according to type of action
		var className = '';
		className = Drupal.settings.consensus.item_options[type]['class'];
		

		// add div container with the style of consensus_item
		if (beforeid) {
			parent_item.find('#'+beforeId).before('<li class="'+className+' consensus_item c_edited"><div class="revision  c_'+type+' current top c_edited"></div></li>');
		}
		else {
			parent_item.append('<ul><li class="'+className+' consensus_item c_edited"><div class="revision c_'+type+' current top c_edited"></div></li></ul>');
		}
    
		// get the form from server
		var data = Object();
		data.nid = Drupal.behaviors.consensus.prototype.nid;
		data.action = 'insert_item';
		data.type = type;
		if ($('#'+parentid).hasClass('consensus_item')) {
			temp = parentid.split('item');
			data.parentid = temp[1];
			temp = $('#'+parentid).children('.revision:visible').attr('id');
			temp = temp.split('r');
			data.parentrevid = temp[1];
		} else {
			data.parentid = '';
		}
		if (beforeid){
			temp = beforeid.split('item');
			data.beforeid = temp[1];
		} else {
			data.beforeid = '';
		}
		var dataString = $.toJSON(data);
		var url = Drupal.settings.basePath+'consensus/form/'+data.nid; // Drupal path needs to be defined
		parent_item.children('.revision:visible').after(Drupal.behaviors.consensus.prototype.loadingGif(true));
		
		//$('.slide_wrapper:visible').slideToggle(500);
		jQuery.post( url, {data:dataString}, function(data){ 
			Drupal.behaviors.consensus.prototype.loadingGif(false);
			Drupal.behaviors.consensus.prototype.generateAddButtons(false);
			// insert the form
		  parent_item.find('.c_edited').children('.revision').append(data);		
			// resize the text area according to contents
			$('.c_textarea, #edit-note').autoResize({
				// On resize:
				onResize : function() {
					$(this).css({opacity:0.8});
				},
				// After resize:
				animateCallback : function() {
					$(this).css({opacity:1});
				},
				// Quite slow animation:
				animateDuration : 300,
				// More extra space:
				extraSpace : 40
			});
			$('textarea.c_textarea').keydown();
		}); // send post end tag

	};

Drupal.behaviors.consensus.prototype.newEndorse = function (action, itemid, revid) {
			var revision = $('#'+itemid).children('.revision:visible');
			var temp = Array();
			
			if (revision.children('textarea.c_textarea').length == 0) {

				// get the form from server
				var data = Object();
				data.action = action;
				temp = itemid.split('item');
				data.itemid = temp[1];
				data.nid = Drupal.behaviors.consensus.prototype.nid;
				data.revid = revid;
				
				temp = $('#'+itemid).parent().closest('.consensus_item').attr('id');
				if (temp) {
					temp = temp.split('item');
					data.parentid = temp[1];
					temp = $('#'+itemid).parent().closest('.consensus_item').children('.revision:visible').attr('id');
					temp = temp.split('r');
					data.parentrevid = temp[1];
				}
				var dataString = $.toJSON(data);
				var url = Drupal.settings.basePath+'consensus/form/'+data.nid; // Drupal path needs to be defined
	
				jQuery.post( url, {data:dataString}, function(data){ 
					// insert the form
					revision.append(data);
				}); // end tag of resize callback
			}
	};

	// function for creating a textbox to submit revisions
	// @param action - name of the action to be performed
	// @param itemid - the itemid on which to perform the action
	// @param oldtext - the content of the text to be overwritten
Drupal.behaviors.consensus.prototype.newTranslation = function(action, itemid, oldText, beforeid){
			
			var revision = $('#'+itemid).children('.revision:visible');
			if (revision.children('textarea.c_textarea').length == 0) {				
				
				var temp = Array();
		
				// get the form from server
				var data = Object();
				temp = itemid.split('item');
				data.itemid = temp[1];
				data.nid = Drupal.behaviors.consensus.prototype.nid;
				temp = $('#'+itemid).parent().closest('.consensus_item').attr('id');
				if (temp) {
					temp = temp.split('item');
					data.parentid = temp[1];
					temp = $('#'+itemid).parent().closest('.consensus_item').children('.revision:visible').attr('id');
					temp = temp.split('r');
					data.parentrevid = temp[1];
				}
				data.action = action;
				data.revid = revision.attr('id').replace('i'+data.itemid+'r', '');
				data.oldtext = oldText;
				if (beforeid) {
					temp = beforeid.split('item');
					data.beforeid = temp[1];
				}
				data.type = revision.find('.data-type').text();
				var dataString = $.toJSON(data);
				var url = Drupal.settings.basePath+'consensus/form/'+data.nid; // Drupal path needs to be defined
				//$('.slide_wrapper:visible').slideToggle(500);
				revision.append(Drupal.behaviors.consensus.prototype.loadingGif(true));
				jQuery.post( url, {data:dataString}, function(data){ 
					Drupal.behaviors.consensus.prototype.generateAddButtons(false);
					// insert the form
					Drupal.behaviors.consensus.prototype.loadingGif(false);
					//revision.children('.data-text').css('display', 'none');
					revision.append(data);
					
					if (Drupal.behaviors.attachWysiwyg) {
						Drupal.behaviors.attachWysiwyg($('#consensus-page-form'));
					} else {
			
						$('.c_textarea, #edit-note').autoResize({
							
							animate : true,
							// On resize:
							onResize : function() {
								$(this).css({opacity:1});
							},
							// After resize:
							animateCallback : function() {
								$(this).css({opacity:1});
							},
							// Quite slow animation:
							animateDuration : 200,
							// More extra space:
							extraSpace : 30
						}); 
					}
					// simulate a keydown to resize the textarea to fit its contents
					$('.c_textarea').keydown();
	
		  }); // end tag of resize callback
		};
	}
/**
 *	Binds click events to various items on the page
 */
Drupal.behaviors.consensus.prototype.clickEvents = function (){
};

/**
 *	Variable stores the ids of all revision items for fast selection
 */
Drupal.behaviors.consensus.prototype.revisionIds = Array();

/**
 *	gets the revision ids for fast jquery selection
 */
Drupal.behaviors.consensus.prototype.setRevisionIds = function () {
	Drupal.behaviors.consensus.prototype.revisionIds = Array();
	
	var revisions = $('.revision');
	for (i=0; i<revisions.length; i++) {
		Drupal.behaviors.consensus.prototype.revisionIds[i] = revisions[i].id;
	}
	
};
