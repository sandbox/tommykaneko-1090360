// JavaScript Document

Drupal.behaviors.consensus.prototype.initiateToolbox = function () {
	
	// initiate the toolbox javascript
	if (Drupal.settings.consensus_toolbox) {
		// bind the change of checkbox to a function
		$('#consensus-toolbox input.c_toolbox').click(function () { Drupal.behaviors.consensus.prototype.clickToolbox(this);});
		 Drupal.behaviors.consensus.prototype.clickToolbox();
		// add insertAll box
		//insertAllBox();
	}
};

/*
*	Javascript functions for the consensus toolbox block
*/
Drupal.behaviors.consensus.prototype.toolboxSettings = Array();

Drupal.behaviors.consensus.prototype.insertAllBox = function () {
	$('#consensus-toolbox form').append('<div class="form-item"><label for="edit-all" class="option"><input type="checkbox" class="form-checkbox" checked="checked" value="1" id="edit-all" name="toolbox-item"> View All</label>	</div>');
		
	$('#edit-all').click( function (){
		var checkedValue = this.checked;
																	
		$('#consensus-toolbox input.c_toolbox').each(function() {  
			this.checked = checkedValue;
			Drupal.behaviors.consensus.prototype.clickToolbox(this);
		});
	});
};

/*
* function returns whether the toolbox checkbox is checked
*/
Drupal.behaviors.consensus.prototype.toolboxIsChecked = function (type) {
	var name = 'toolbox-'+type ;
	return $('#consensus-toolbox input[name="'+name+'"]').is(':checked');
};

/*
*	function displays or hides the items on click on the toolbox checkboxes
* TODO: change approach from the display:none/block to a class-based approach to
*       showing and hiding items
*/
Drupal.behaviors.consensus.prototype.clickToolbox = function (target) {
	// build the the list of classes to be shown
	$('#consensus-toolbox input.c_toolbox').each(function(){
		var itemType = $(this).attr('name').replace('toolbox-', '');
		var value = $(this).is(':checked');
		
		Drupal.behaviors.consensus.prototype.toolboxSettings[itemType] = value;
	});
	
	for (var itemType in Drupal.behaviors.consensus.prototype.toolboxSettings) {
		if (itemType == 'blocked') {
			if (Drupal.behaviors.consensus.prototype.toolboxSettings[itemType]) {
				$('.c_content').addClass('toolbox-blocked-reveal');
			} else {
				$('.c_content').removeClass('toolbox-blocked-reveal');
			}
		} else if (itemType == 'unendorsed') {
			if (Drupal.behaviors.consensus.prototype.toolboxSettings[itemType]) {
				$('.exclude').addClass('unendorse-reveal');
			} else {
				$('.unendorse-reveal').removeClass('unendorse-reveal');
			}
		} else {
			if (Drupal.behaviors.consensus.prototype.toolboxSettings[itemType]) {
				$('.c_content').removeClass('toolbox-hide-'+itemType);
			} else {
				$('.c_content').addClass('toolbox-hide-'+itemType);
			}	
		}
		
	} 
/*		
	
	
	// if users block is disabled
	if (Drupal.settings.consensus_users  == null) {
		// show and hide the item types
		for (itemType in toolboxSettings) {
			if (toolboxSettings[itemType]) {
				$('.c_content .c_'+itemType).css('display', '')
			} else {
				$('.c_content .c_'+itemType).css('display', 'none')
			}	
		} 
	}
	// if user block is enabled
	else {
		consensus_users.includeExclude();
		for (itemType in toolboxSettings) {
			if (!toolboxSettings[itemType]) {
				$('.c_content .c_'+itemType).css('display', 'none');
			}	
			if (itemType == 'blocked' && toolboxSettings[itemType]) {
				$('.c_content .c_'+itemType).css('display', '');
			}
			if (itemType == 'unendorsed' && toolboxSettings[itemType]) {
				$('.c_content .c_'+itemType).css('display', '');
			}
		} 
	}
	*/
}



