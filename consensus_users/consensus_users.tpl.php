<?php
// $Id
//print drupal_get_form('consensus_users_form');
?>
<div id="consensus-users">
  <form id="<?php print $form['#id'].'-'.$form['#sorted']; ?>" method="<?php print $form['#method']; ?>" accept-charset="UTF-8" action="<?php print $form['#action']; ?>"><?php
  print drupal_render($form['form_build_id']);
  print drupal_render($form['form_id']);
  print drupal_render($form['form_token']);
  print drupal_render($form['sortby']);
  print drupal_render($form['sort-order']);
	print theme('consensus_users_sortby', $sortby);
  ?>
  <ul class="consensus_user">
    <?php
    foreach ($info['users'] as $id => $account) {
			$uid = $account['user']->uid;
      ?>
    <li id="uid-<?php print $uid;?>" class="c_user"> 
        <div class = "c_user_wrapper">
        	<?php print $account['user']->picture_html; ?>
          <div class="c_user_name"><?php if ($account['user']->realname) { print $account['user']->realname;} else { print $account['user']->name;} ?>
            <span class="small"><?php print l(t('profile...'),'user/'.$uid, array('alias'=>TRUE)); ?></span></div>
        <fieldset class="collapsible<?php if ($info['data']['minimize'][$uid] || !isset($info['data']['minimize'][$id])){ ?> collapsed<?php }else{ }?>">
          <legend>More</legend>
        <div class="c_user_info">
          <?php if ($account['user']->og_groups){ 
        ?>
          <ul class="c_groups">
            <?php
        $i = 0;
        foreach ($account['user']->og_groups as $nid => $group) {
          if ($i < 4) { // set the maximum number of groups to display
          ?>
            <li><?php print l($group['title'], 'node/'.$nid ); ?></li>
            <?php
          } elseif (!isset($a)) {
          ?>
            <li><?php print l(t('more...'), 'user/'.$uid ); ?></li>
            <?php
          $a = 1;
          }
          $i++;
        } 
        ?>
          </ul>
          <?php
        } ?>
        	<!-- Put in users stats -->
          <ul class="c_items">
            <?php
						$c = 0;
              foreach ($account['count'] as $type => $count) {
            		if ($count['author']['current'] && $type != 'heading') {?>
            <li class="c_<?php print $type; ?>" title="<?php print $type;?>"><img src="<?php print url(drupal_get_path('module', 'consensus') . '/css/images/bullet-' . $type . '.png');?>" alt="<?php print $type;?>"/>x<?php print $count['author']['current'];?></li>
            <?php }
						
					
          $c = $c + intval($count['endorse']['current']);
          } ?>
            <li class="c_authored_endorsed" title="<?php print t('endorsed/author ratio');?>"><img src="<?php print url(drupal_get_path('module', 'consensus') . '/css/images/endorsed-authored.png');?>" alt="<?php print t('endorsed/author ratio');?>"/><?php print $account['count']['endorsed']['all'] ? $account['count']['endorsed']['all'] : 0 ;?>/<?php print $account['count']['authored']['all'] ? $account['count']['authored']['all'] : 0;?></li>
          </ul>
        </div>        
        </fieldset>
      <?php print drupal_render($form['consensus_user'][$uid]); ?>
      </div> <!-- end of c_user_wrapper -->
    </li>
    <?php } ?>
  </ul>
  <?php print drupal_render($form['submit']); ?>
  </form>
</div>