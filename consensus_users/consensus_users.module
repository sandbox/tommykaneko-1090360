<?php

/**
* Implementation of hook_node_info().
*/

/**
* Access Permission of this module by hook_perm();
*/
function consensus_users_perm() {
  return array('access consensus_users block', 'save consensus view');
}


function consensus_users_theme() {
	return array(
    'consensus_users' => array(
      'template' => 'consensus_users',
      'arguments' => array('info' => NULL, 'node' => NULL, 'sortby' => NULL, 'users_per_page' => NULL, 'form' => NULL),
    ),
    'consensus_users_sortby' => array(
      'arguments' => array('sortby' => NULL),
    ),
  );
}
/*
function consensus_users_init () {
	
}
*/

function consensus_users_menu() {
	$items['admin/settings/consensus/users'] = array (
		'title' => 'Consensus Users Configuration', 
		'description' => 'Configure the Consensus Users Settings',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('consensus_users_admin_settings'),
		'access arguments' => array('administer site configuration'), 
		'type' => MENU_LOCAL_TASK,
  );
	
  return $items;
}

/*
*  Impementation of hook_form_alter
*/
function consensus_users_form_alter (&$form, &$form_state, $form_id) {
  
  
  if (arg(2) == 'block' && arg(3) == 'configure' && arg(4) == 'consensus_users' && $form_id == 'block_admin_configure') {
    $module = arg(4);
	$delta = arg(5);
  
  $edit = db_fetch_array(db_query("SELECT pages, visibility, custom, title FROM {blocks} WHERE module = '%s' AND delta = '%s'", $module, $delta));
  $form['block_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Title'),
    '#collapsible' => TRUE,
  );
  $form['block_settings']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block title'),
    '#maxlength' => 64,
    '#description' => $module == 'block' ? t('The title of the block as shown to the user.') : t('Override the default title for the block. Use <em>&lt;none&gt;</em> to display no title, or leave blank to use the default block title.'),
    '#default_value' => $edit['title'],
    '#weight' => -18,
  );
  }
}

function consensus_users_settings ($node = NULL) {
  $settings = array(
    'buttons' => array (
			'include' => array (
				'title' => t('Included'),
				'default' => true,
			),
		),
		'users' => consensus_users_get_users ($node), // TODO this function is called twice - consider not doing so
  );
  return $settings;
}

/**
* Implementation of hook_block().
*/

function consensus_users_block($op='list', $delta=0) {
	if (!module_exists('consensus')) {
		return;
	}
	// listing of blocks, such as on the admin/block page
  switch ($op) {
	case 'list':	
	  $block[0]['info'] = t('Consensus Users Block');
	return $block;
	case 'view':
		// only display block if on consensus page
		if (!$node && (arg(0) == 'node') && is_numeric(arg(1))) {
				$node = node_load(arg(1));
		}
		if (consensus_is_consensus_page($node)) {
			// get data containing the user's settings for this node
			global $user;
			if ($user->uid){
				$result = db_query("SELECT data FROM {consensus_users_settings} WHERE uid=%d AND nid=%d", $user->uid, $node->nid);
				$data = unserialize(db_result($result));
			} else {
				$data = NULL;
			}
			// set sorting method
			if ($_GET['user_order']) {
				$sortby = $_GET['user_order'];
			} else {
				$sortby = 'custom';
			}
			$users = consensus_users_get_users ($node, $sortby);
			
			switch ($delta) {
				// users block
				case 0:
					// send settings to javascript
					drupal_add_js(array('consensus_users' => $users), 'setting');
					drupal_add_js(drupal_get_path('module', 'consensus_users') . '/consensus_users.js');
					
					$info = array (
						'users' => $users,
						'sortby' => $sortby,
						'settings' => consensus_settings($node),
						'data' => $data,
					);
		
					global $form;
					$form = consensus_users_populate_form('consensus_users_form');
					//$form = drupal_retrieve_form('consensus_users_form', $form, $form_state);
					//drupal_prepare_form('consensus_users_form', $form, $form_state);
					//drupal_process_form('consensus_users_form', $form, $form_state);
					
					//$form = drupal_get_form('consensus_users_form');
					$block['subject'] = t('People');
					$block['content'] = theme('consensus_users', $info, $node, $sortby, NULL, $form);
				break;
			}
			return $block;
		}
  }
}
/* get an array of all user ids in a particular node
*  @param $node - node object, or nid
*/
function consensus_users_get_users ($node = NULL, $sortby = 'endorsed-author-all') {
	
	if (!consensus_is_consensus_page()) {
		return;
	}
	
	if (!isset($node->consensus)) {
		// get node
		if ((arg(0) == 'node') && is_numeric(arg(1))) {
			if (!is_object($node)) {
				$node = node_load(arg(1));
			}
		}
		else {
			return false;
		}
		if (isset($node->consensus)) {
			$consensus = $node->consensus;
		} elseif (_consensus_check_xml_support()) {
			require_once(drupal_get_path('module', 'consensus').'/consensus_class.php');
			$consensus = new consensus($node, NULL); // create new XML object
		} else {
			watchdog('consensus_users',"XML/XSLT PHP support misconfigured", array(), WATCHDOG_ERROR);
			return false;
		}
	} else {
		$consensus = $node->consensus;
	}
	 
	// construct the user information array
	$users = array();
	
	$array = array();

	$items = $consensus->simpleXML->XPath('content//item');

	foreach ($items as $item) {

		foreach ($item as $revision) {
			if ($revision->getName() == 'revision') {
				// add author information
				$users[intval($revision->author->user->id)]['items'][check_plain($revision->attributes()->type)][] = array (
					'author' => 1,
					'current' => check_plain($revision->attributes()->current),
					'timestamp' => check_plain($revision->attributes()->timestamp),
					'itemid' => check_plain($item->attributes()->id), 
					'revid' => check_plain($revision->attributes()->revid)
				);
				// add count information
				$users[intval($revision->author->user->id)]['count'][check_plain($revision->attributes()->type)]['author']['all']++;
				$users[intval($revision->author->user->id)]['count']['authored']['all']++;
				if ($revision->attributes()->current == 1) {
					$users[intval($revision->author->user->id)]['count'][check_plain($revision->attributes()->type)]['author']['current']++;
					$users[intval($revision->author->user->id)]['count']['authored']['current']++;
				}
		
				// add endorse information
				$endorses = $revision->XPath('endorse[@active=1]');
				foreach ($endorses as $endorse) {
					$contexts = array();
					foreach ($endorse->XPath('context') as $i => $context) {
						$contexts[$i]['action'] = $context->attributes()->action;
						$contexts[$i]['parentId'] = $context->attributes()->parentId;
						$contexts[$i]['parentRevId'] = $context->attributes()->parentRevId;
						$contexts[$i]['timestamp'] = $context->attributes()->timestamp;
					}
					$users[intval($endorse->user->id)]['items'][check_plain($revision->attributes()->type)][] = array (
						'endorse' => 1,
						'context' => $contexts,
						'current' => check_plain($revision->attributes()->current),
						'timestamp' => check_plain($endorse->attributes()->timestamp),
						'itemid' => check_plain($item->attributes()->id), 
						'revid' => check_plain($revision->attributes()->revid)
					);
					
					// add count information
					$users[intval($endorse->user->id)]['count'][check_plain($revision->attributes()->type)]['endorse']['all']++;
					// only include endorsements from others?
					//if (intval($endorse->user->id) != intval($revision->author->user->id))
						$users[intval($revision->author->user->id)]['count']['endorsed']['all']++;
					if ($revision->attributes()->current == 1) {
						$users[intval($endorse->user->id)]['count'][check_plain($revision->attributes()->type)]['endorse']['current']++;
						//// only include endorsements from others
						//if (intval($endorse->user->id) != intval($revision->author->user->id))
							$users[intval($revision->author->user->id)]['count']['endorsed']['current']++;
					}
				}
			}
		}
	}
  
  // populate the user object 
  foreach ($users as $uid => $account) {	
  	if ($user_load = user_load($uid)) {
			
			//unset sensitive information
			$allowed = array('name', 'realname', 'picture', 'language', 'created', 'signature', 'uid', 'status', 'contact', 'profile_real_name', 'profile_anonymous', 'og_groups');
			foreach ($user_load as $key => $item) {
				if (!in_array($key, $allowed)) {
					unset($user_load->$key);
				}
			}
			
			$users[$uid]['user'] = $user_load;
			
			// profile pictures
			if (module_exists('imagecache_profiles') && variable_get('user_picture_imagecache_comments', false)) {
				if ($users[$uid]['user']->picture) {
					$users[$uid]['user']->picture_html = theme('imagecache', variable_get('user_picture_imagecache_comments', false), $users[$uid]['user']->picture, $users[$uid]['user']->profile_real_name.'\'s Picture', $users[$uid]['user']->profile_real_name, array('class' => 'user_img'));
				} else {
					$users[$uid]['user']->picture_html = theme('imagecache', variable_get('user_picture_imagecache_comments', false), variable_get('user_picture_default', ''), $users[$uid]['user']->profile_real_name.'\'s Picture', $users[$uid]['user']->profile_real_name, array('class' => 'user_img'));
				}
			} else {
				$users[$uid]['user']->picture_html = theme('profile', $users[$uid]['user']);
			}
		}
  }
	
	if ($sortby = check_plain($sortby)) {
		$users = consensus_users_sortby ($users, $sortby);
	}
		
  return $users;
}


/*
*	Themes sorting pane
* @param - $sortby - the active sorting method
*	returns themed sortby pane
*/

function theme_consensus_users_sortby ($sortby) {
	$sort_options = array (
		'custom' => array('name' => t('Custom'), 'image' => 'bullet-custom.png'),
		'endorsed-author-all' => array('name' => t('Endorsed/Author ratio'), 'image' => 'endorsed-authored.png'),
		'endorsed-current' => array('name' => t('No of endorsements for authored items'), 'image' => 'bullet-endorse.png'),
		'author-current' => array( 'name' => t('No. of active contributions'), 'image' => 'bullet-edit.png'),
		//'author-all' => t('No. of all previous contributions'),
		//'endorsed-author-current' => t('Endorsed/Author ratio of only current items'),
		//'item-all' => t('No. of authored items'),
		'item-current' => array('name' => t('No of active items'), 'image' => 'bullet-item.png'),
	);
	
	
	
	$output = '<div id="c_user_sortby">';
	$output .= '<strong>'.t('sort by:').'</strong>';
	$output .= '<ul>';
	foreach ($sort_options as $key => $value) {
		if ($key == $sortby) {
			$class = ' class="c_active_sort"';
		} else {
			$class = '';
		}
		$output .= '<li'.$class.'><a title="'.$value['name'].'" href="'. url(arg(0).'/'.arg(1) , array('query' => array('user_order' => $key))) .'" name="'.$key.'"><img src="'.url(drupal_get_path('module', 'consensus') . '/css/images/' . $value['image']).'" alt="'.$value['name'].'" /></a></li>';
	}
	$output .= '</ul>';
	$output .= '</div>';
	return $output;
}

/*
*	Sorts $users array by specified method
*	@param $users
* @param $sortby
*		string of sorting method. can be:
*							-'custom'
*							-'endorsed-author-all'
*							-'endorsed-author-current'
*							-'author-current'
*							-'author-all'
*							-'endorsed-current'
*							-'endorsed-all'
*	@param - Constant - SORT_DESC or SORT_ASC
*/
function consensus_users_sortby ($users, $sortby = 'endorsed-author-all', $order=SORT_DESC) {
	$sorted_users = array();
	$sortable_array = array();
	switch ($sortby) {
		// sort by user's own sort order
		case 'custom':
			global $user;
			if ((arg(0) == 'node') && is_numeric(arg(1))) {
				if (!is_object($node)) {
					$node = node_load(arg(1));
				}
			}
			$data = consensus_users_get_data($node->nid, $user->uid);
			$uids = str_replace('uid[]=', '', $data['sort-order']);
			$custom_sort = explode('&', $uids);
			
			if (count($custom_sort) > 1) {
				foreach ($custom_sort as $k => $v) {
					$value = intval($v);
					$sortable_array[$value] = $k;
				}
				$order = SORT_ASC;
			} else {
				foreach ($users as $k => $v) {
					if ($v['count']['authored']['current']) {
						$sortable_array[$k] = $v['count']['authored']['current'];
					} else {
						$sortable_array[$k] = 0;					
					}
				}
			}
			foreach ($users as $us) {
				if (!in_array($us['user']->uid, $custom_sort)) {
					$sortable_array[$us['user']->uid] = 0;
				}
			}
			
						
			break;
		// sort by most number of current authored items
		case 'author-current':
		
			foreach ($users as $k => $v) {
				if ($v['count']['authored']['current']) {
					$sortable_array[$k] = $v['count']['authored']['current'];
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		// sort by most number of all authored items
		case 'author-all':
		
			foreach ($users as $k => $v) {
				if ($v['count']['authored']['all']) {
					$sortable_array[$k] = $v['count']['authored']['current'];
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'endorsed-author-all':
		
			foreach ($users as $k => $v) {
				if ($v['count']['authored']['all']) {
					$sortable_array[$k] = intval($v['count']['endorsed']['all']) / intval($v['count']['authored']['all']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'endorsed-author-current':
		
			foreach ($users as $k => $v) {
				if ($v['count']['authored']['all']) {
					$sortable_array[$k] = intval($v['count']['endorsed']['current']) / intval($v['count']['authored']['current']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'item-all':
		
			foreach ($users as $k => $v) {
				if ($v['count']['item']['author']['all']) {
					$sortable_array[$k] = intval($v['count']['item']['author']['all']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'item-current':
			foreach ($users as $k => $v) {
				if ($v['count']['item']['author']['current']) {
					$sortable_array[$k] = intval($v['count']['item']['author']['current']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'endorsed-current':
		
			foreach ($users as $k => $v) {
				if ($v['count']['endorsed']['current']) {
					$sortable_array[$k] = intval($v['count']['endorsed']['current']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
		case 'endorsed-all':
		
			foreach ($users as $k => $v) {
				if ($v['count']['endorsed']['all']) {
					$sortable_array[$k] = intval($v['count']['endorsed']['all']);
				} else {
					$sortable_array[$k] = 0;					
				}
			}
			break;
	}

	// sort the temporary array
	switch ($order) {
			case SORT_ASC:
					asort($sortable_array);
			break;
			default :
					arsort($sortable_array);
	}
	// sort the users array
	foreach ($sortable_array as $k => $v) {
			$sorted_users[$k] = $users[$k];
	}

	return $sorted_users;
}


function consensus_users_form (&$form_state) {	
	if (!consensus_is_consensus_page()) {
		return;
	}
	$users = consensus_users_get_users();
	if ($users) {
		if (!is_array($users)) {
			$users = array ($users => NULL);
		}
		global $user;
		if ((arg(0) == 'node') && is_numeric(arg(1))) {
			if (!is_object($node)) {
				$node = node_load(arg(1));
			}
		}
		$data = consensus_users_get_data($node->nid, $user->uid);

		$uids = '';
		if (is_array($users)) {
			$form['nid'] = array(
				'#type' => 'hidden',
				'#value' => $node->nid,
			);
			foreach ($users as $key => $value) {
				/*
				// store uid information as a string, to be split later.
				$uids = $uids.';'.$key;
				
				$form['consensus_user'][$key]['uids'] = array(
					'#type' => 'hidden',
					'#value' => $uids,
				);
				*/
				$form['consensus_user'][$value['user']->uid]['include'] = array(
					'#type' => 'checkbox',
					'#title' => '',
					'#default_value' => isset($data['include'][$value['user']->uid]) ? $data['include'][$value['user']->uid]: 1,
					'#return_value' => 1,
					'#name' => 'include-'.$value['user']->uid,
					'#attributes' => array('class' => 'c_include'),
					//'#options' => $options,
					//'#description' => t('The log.'),
				);
				$form['consensus_user'][$value['user']->uid]['minimize'] = array(
					'#type' => 'hidden',
					'#title' => t('Include'),
					'#value' => isset($data['minimize'][$value['user']->uid]) ? $data['minimize'][$value['user']->uid]: 0,
					'#name' => 'minimize-'.$value['user']->uid,
					'#attributes' => array('class' => 'c_minimize'),
					//'#options' => $options,
					//'#description' => t('The log.'),
				);
				
			}
		}
	}
	$form['sort-order'] = array(
		'#type' => 'hidden',
		'#value' => is_string($data['sort-order']) ? $data['sort-order']: '',
	);
	$form['sortby'] = array(
		'#type' => 'hidden',
		'#value' => is_string($_GET['user_order']) ? $_GET['user_order']: (is_string($data['sortby']) ? $data['sortby']: 'endorse-author-all'),
	);
	if (user_access('save consensus view')) {
		$form['submit'] = array('#type' => 'submit', '#value' => t('Save View'));
	}
	
	return $form;
}


function consensus_users_get_data ($nid = NULL, $uid = NULL) {
	if ($nid && $uid) {
		return unserialize( db_result( db_query("SELECT data FROM {consensus_users_settings} WHERE uid=%d AND nid=%d", $uid, $nid)));
	} else {
		return false;
	}
}

function consensus_users_form_submit($form, &$form_state) {

	if (user_access('save consensus view')) {
		global $user;
		//watchdog('consensus_users', print_r($form['#post'], true));

		// prepare the data array to insert into the database
		$data = array();
		
		foreach ($form['consensus_user'] as $uid => $value) {
			if (is_int($uid)){
				// get the include information
				if ($form['#post']['include-'.$uid] == 1) {
					$data['include'][$uid] = 1;
				} else {
					$data['include'][$uid] = 0;
				}
				// get the minimize information
				$data['minimize'][$uid] = $form['#post']['minimize-'.$uid];
			}
		}
		$data['sortby'] = $form['#post']['sortby'];
		$nid = $form['nid']['#value'];
		$data['sort-order'] = $form['#post']['sort-order'];
				
		$result = db_query("SELECT data FROM {consensus_users_settings} WHERE uid=%d AND nid=%d", $user->uid, $nid);
		
		// if entry exists, merge the new and old data TODO: it doesn't quite merge them yet
		if ($old_data = db_result($result)) {
			// merge the data from old and new
			$old_data = unserialize($old_data);
			$old_data['include'] = $data['include'];
			$old_data['minimize'] = $data['minimize'];
			if ($data['sortby'] == 'custom') {
				$old_data['sort-order'] = $data['sort-order'];
			}
			$data = serialize($old_data);
			//update the database row
			$result = db_query("UPDATE {consensus_users_settings} 
			SET data='%s', changed='%s'
			WHERE uid=%d AND nid=%d", $data, time(), $user->uid, $nid);
			
		}
		// else create a new entry in the database
		else {
			$data = serialize($data);
			$result = db_query("INSERT INTO {consensus_users_settings} (uid, nid, data, changed) 
		VALUES (%d, %d, '%s', '%s')", $user->uid, $nid, $data, time());
		}

		if ($form['#post']['sortby']) {
			$form_state['redirect'] = array('node/' . intval($form['nid']['#value']) , 'user_order=' . check_plain($form['#post']['sortby']));
		} 
		
		return;
	}
}


// copied from drupal_get_form - the form is not rendered, but the array is returned for theming.
function consensus_users_populate_form($form_id) {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);

  $args = func_get_args();
  $cacheable = FALSE;

  if (isset($_SESSION['batch_form_state'])) {
    // We've been redirected here after a batch processing : the form has
    // already been processed, so we grab the post-process $form_state value
    // and move on to form display. See _batch_finished() function.
    $form_state = $_SESSION['batch_form_state'];
    unset($_SESSION['batch_form_state']);
  }
  else {
    // If the incoming $_POST contains a form_build_id, we'll check the
    // cache for a copy of the form in question. If it's there, we don't
    // have to rebuild the form to proceed. In addition, if there is stored
    // form_state data from a previous step, we'll retrieve it so it can
    // be passed on to the form processing code.
    if (isset($_POST['form_id']) && $_POST['form_id'] == $form_id && !empty($_POST['form_build_id'])) {
      $form = form_get_cache($_POST['form_build_id'], $form_state);
    }

    // If the previous bit of code didn't result in a populated $form
    // object, we're hitting the form for the first time and we need
    // to build it from scratch.
    if (!isset($form)) {
      $form_state['post'] = $_POST;
      // Use a copy of the function's arguments for manipulation
      $args_temp = $args;
      $args_temp[0] = &$form_state;
      array_unshift($args_temp, $form_id);

      $form = call_user_func_array('drupal_retrieve_form', $args_temp);
      $form_build_id = 'form-'. md5(uniqid(mt_rand(), TRUE));
      $form['#build_id'] = $form_build_id;
      drupal_prepare_form($form_id, $form, $form_state);
      // Store a copy of the unprocessed form for caching and indicate that it
      // is cacheable if #cache will be set.
      $original_form = $form;
      $cacheable = TRUE;
      unset($form_state['post']);
    }
    $form['#post'] = $_POST;

    // Now that we know we have a form, we'll process it (validating,
    // submitting, and handling the results returned by its submission
    // handlers. Submit handlers accumulate data in the form_state by
    // altering the $form_state variable, which is passed into them by
    // reference.
    drupal_process_form($form_id, $form, $form_state);
    if ($cacheable && !empty($form['#cache'])) {
      // Caching is done past drupal_process_form so #process callbacks can
      // set #cache.
      form_set_cache($form_build_id, $original_form, $form_state);
    }
  }

  // Most simple, single-step forms will be finished by this point --
  // drupal_process_form() usually redirects to another page (or to
  // a 'fresh' copy of the form) once processing is complete. If one
  // of the form's handlers has set $form_state['redirect'] to FALSE,
  // the form will simply be re-rendered with the values still in its
  // fields.
  //
  // If $form_state['storage'] or $form_state['rebuild'] has been set
  // and input has been processed, we know that we're in a complex
  // multi-part process of some sort and the form's workflow is NOT
  // complete. We need to construct a fresh copy of the form, passing
  // in the latest $form_state in addition to any other variables passed
  // into drupal_get_form().

  if ((!empty($form_state['storage']) || !empty($form_state['rebuild'])) && !empty($form_state['process_input']) && !form_get_errors()) {
    $form = drupal_rebuild_form($form_id, $form_state, $args);
  }

  // If we haven't redirected to a new location by now, we want to
  // render whatever form array is currently in hand.
  return $form;
}

/*
 *	Admin settings
 *
 */

function consensus_users_admin_settings () {
 // return system_settings_form($form);
}


function consensus_users_settings_form_submit($form, &$form_state) {
}
