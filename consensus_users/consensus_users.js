// JavaScript Document

/**
 *	Initiate the Consensus Users javascript functions
 */
Drupal.behaviors.consensus.prototype.initiateUsers = function (context) {
	// is there a consensus_users block?
	var users_block = $('.c_user');
	// initiate the consensus users javascript if module is installed
	if (Drupal.settings.consensus_users != null && users_block.length != 0) {
		
		// set User Ids for quick jQuery retrieval
		Drupal.behaviors.consensus.prototype.setUserIds(context);
		// set userEndorsements array
		Drupal.behaviors.consensus.prototype.setUserEndorsements(context);
		
		var userIds = Drupal.behaviors.consensus.prototype.userIds;
		
		$('.c_user').hover(function(){
			Drupal.behaviors.consensus.prototype.highlightItems(this); // on hovering over user names, highlight over the endorsed items
		}, function() {
			Drupal.behaviors.consensus.prototype.highlightItems(false);
		});
		Drupal.behaviors.consensus.prototype.highlightUsers(context); // on hovering over items, highlight the users who endorsed it
		Drupal.behaviors.consensus.prototype.updateItemContributions(context);
		Drupal.behaviors.consensus.prototype.updateAllContributions(context);
		Drupal.behaviors.consensus.prototype.includeExclude();
		// when page initialises, perform action
		
		for (i=0; i<userIds.length; i++) {
			Drupal.behaviors.consensus.prototype.includeHighlight('#'+userIds[i]);
		}
		$('.c_user .c_include:checkbox').change(function(){
			Drupal.behaviors.consensus.prototype.updateAllContributions(context);
			//Drupal.behaviors.consensus.prototype.updateContribution(this)
			Drupal.behaviors.consensus.prototype.includeExclude();
			Drupal.behaviors.consensus.prototype.includeHighlight(this);
		});
		Drupal.behaviors.consensus.prototype.collapse(context);
		Drupal.behaviors.consensus.prototype.usersSortOrder();
		Drupal.behaviors.consensus.prototype.usersSortable();
		
		// bind click events
		Drupal.behaviors.consensus.prototype.clickUser(context);
		
	}
}

		// register hooks
		/*
		Hook.register('pageRefresh', 'Drupal.behaviors.consensus.prototype.setUserIds');
		Hook.register('pageRefresh', 'Drupal.behaviors.consensus.prototype.setUserEndorsements');
		Hook.register('pageRefresh', 'Drupal.behaviors.consensus.prototype.updateItemContributions');
		Hook.register('pageRefresh', 'Drupal.behaviors.consensus.prototype.updateAllContributions');
		Hook.register('pageRefresh', 'Drupal.behaviors.consensus.prototype.includeExclude');
*/

/**
 *	Set the shared properties
 */
// array recording the contributions of only checked users for each revision
Drupal.behaviors.consensus.prototype.contributions = Array();
// array recording the id of currently selected contributors
Drupal.behaviors.consensus.prototype.contributors = Array();
// array recording the contributions of users for each revision
Drupal.behaviors.consensus.prototype.itemContributions = Array();  
// array of userIds
Drupal.behaviors.consensus.prototype.userIds = Array();  


Drupal.behaviors.consensus.prototype.clickUser = function (context) {
	// if the context is not the document, return false
	if ($('.c_content', context).length == 0) {
		return false;
	}

	
	$('#consensus-users').click(function(e) {
			 var clicked = $(e.target);
			if (clicked.closest('li.c_user').length != 0) {
				if (clicked.closest('a, input, label').length == 0) {
					var target = clicked.closest('li.c_user');
					Drupal.behaviors.consensus.prototype.collapsibleItems(target);
				}
			}
	});
}
/**
 *	Caches the User IDs for quick jQuery retrieval
 */
Drupal.behaviors.consensus.prototype.setUserIds = function() {
	Drupal.behaviors.consensus.prototype.userIds = Array();  
	
	var users = document.getElementsByClassName('c_user');
	for (i=0; i<users.length; i++) {
		Drupal.behaviors.consensus.prototype.userIds[i] = users[i].id;
	}
}

/**
 *	Produces highlight effects for selected target
 */
Drupal.behaviors.consensus.prototype.highlightItems = function (target) {

	var c_nid = Drupal.behaviors.consensus.prototype.nid;
	if (target) {
		var uid = $(target).closest('.c_user').attr('id');
		var revids = Drupal.behaviors.consensus.prototype.userEndorsements[uid];
		for (var i in revids) {
			var revision = $('#'+i);

			// add the highlight class
			revision.addClass('c_highlight');
			// if user is the author, add c_authored class
			if (revids[i]['author'] == uid) {
				revision.addClass('c_highlight-author');
			}
			// if user has endorsed it, add c_endorsed class
			if (revids[i]['endorse'] != undefined) {
				revision.addClass('c_highlight-endorse');
			}
			
		}
	} else {
			$('.c_highlight').removeClass('c_highlight c_highlight-endorse c_highlight-author');
	}
	return false;
}

Drupal.behaviors.consensus.prototype.collapsibleItems = function (target) {

	if (!$(target).find('fieldset:first').hasClass('stop_collapse')) {
			var fieldset = $(target).find('fieldset:first')[0];
			Drupal.behaviors.consensus.prototype.toggleFieldset(fieldset);
		} else {
			$(target).find('fieldset:first').removeClass('stop_collapse');
		}
	
}
/**
 *	 add/remove highlighting class to the user element if include checkbox is on
 */
Drupal.behaviors.consensus.prototype.includeHighlight = function (target) {
	var target = $(target);
	if (!target.hasClass('c_user')) {
		target = target.closest('.c_user');
	}
	if (target.find('.c_include').is(':checked')) {
		target.addClass('c_highlight_include');
	} else {
		target.removeClass('c_highlight_include');
	}
}


/**
 *	changes an input tag's value with class .c_minimize
 *
 *	@param fieldset
 *		the HTML element conatining the collapsible fieldset
 */
Drupal.behaviors.consensus.prototype.minimizeVal = function (fieldset) {
	if (!$(fieldset).hasClass('c_user')) {
		target = $(fieldset).closest('.c_user');
	} else {
		target = $(fieldset);
	}
	if (target.find('.collapsible').hasClass('collapsed')) {
		target.find('.c_minimize').val(1);
	} else {
		target.find('.c_minimize').val(0);
	}
}


/**
 *	 adds/removes highlighting classes as user hovers over revision items
 */
Drupal.behaviors.consensus.prototype.highlightUsers = function (context) {
	$('div.revision', context).hover(function(){
		var revision = $(this);
		revision.addClass('c_highlight').parent().addClass('c_highlight');															 
		var datas = revision.children('.data').children();
		
		for (i=0; i<datas.length; i++) {
			var data = $(datas[i]);
			if (data.hasClass('data-author')) {
				var uid = data.attr('data-uid');
				$('#uid-'+uid).addClass('c_highlight-author');
			}
			if (data.hasClass('data-endorse')) {
				var uid = data.attr('data-uid');
				$('#uid-'+uid).addClass('c_highlight-endorse');
			}
		}

	}, function() {
		$('.c_highlight').removeClass('c_highlight');
		$('.c_highlight-author').removeClass('c_highlight-author');
		$('.c_highlight-endorse').removeClass('c_highlight-endorse');
	});
}

/**
 *	Builds an Associative array with all endorsements by each user
 */
Drupal.behaviors.consensus.prototype.setUserEndorsements = function () {
	Drupal.behaviors.consensus.prototype.userEndorsements = Array();
	
	var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
	var userEndorsements = Array();
	
	for (i=0; i<revisionIds.length; i++) {
		var revision = $('#'+revisionIds[i]);
		var revisionId = revisionIds[i];
		
		// set author information
		var author = 'uid-'+revision.find('.data-author').attr('data-uid');
		if (!userEndorsements[author]) {
			userEndorsements[author] = Array();
		}
		if (!userEndorsements[author][revisionId]) {
			userEndorsements[author][revisionId] = Array();
		}
		if (!userEndorsements[author][revisionId]['author']) {
			userEndorsements[author][revisionId]['author'] = author;
		}
		
		// set endorse information
		var endorses = revision.find('.data-endorse');
		
		for (a=0; a<endorses.length; a++) {
			var endorse = $(endorses[a]);
			var uid = 'uid-'+endorse.attr('data-uid');
			var contexts = endorse.children('.data-context');
			if (!userEndorsements[uid]) {
				userEndorsements[uid] = Array();
			}
			if (!userEndorsements[uid][revisionId]) {
				userEndorsements[uid][revisionId] = Array();
			}
			if (!userEndorsements[uid][revisionId]['endorse']) {
				userEndorsements[uid][revisionId]['endorse'] = Array();
			}
			for (b=0; b<contexts.length; b++) {
				var context = $(contexts[b]);
				var contextArray = Array();
				var action = context.attr('data-action');

				if (action == 'endorse') {

					contextArray['parentRevId'] = context.attr('data-parentrevid');
					contextArray['parentId'] = context.attr('data-parentid');
					if (!userEndorsements[uid][revisionId][action]) {
						userEndorsements[uid][revisionId][action] = Array();
					}
					userEndorsements[uid][revisionId][action].push(contextArray);
				}
			}
		}
	}
	Drupal.behaviors.consensus.prototype.userEndorsements = userEndorsements;
}

/*
*		builds from scratch the "contributions" variable from the contributions of each user that is "included"
*/
Drupal.behaviors.consensus.prototype.updateAllContributions = function () {
	// reset the variables!
	Drupal.behaviors.consensus.prototype.contributions = Array();
	Drupal.behaviors.consensus.prototype.contributors = Array();
	
	// create an array representing all the items on the page, built from the contributions of each user
	var userIds = Drupal.behaviors.consensus.prototype.userIds;
	
	Drupal.behaviors.consensus.prototype.updateContribution(userIds);	

};

/*
*		modifies the "contributions" variable for the Consensus user element
*/
Drupal.behaviors.consensus.prototype.updateContribution = function (userIds) {
		// if singe user Id is passed, check the string
		if (typeof userIds == 'string') {
			var userIds = new Array(userIds);  
		}
		var nid = Drupal.behaviors.consensus.prototype.nid;
		var userEndorsements = Drupal.behaviors.consensus.prototype.userEndorsements;
		
		for (var a in userIds) {
			var target = $('#'+userIds[a]).closest('.c_user');
			var uid = userIds[a];
			var checked = target.find('input.c_include').is(':checked');
			if (checked) {
				for (var revid in userEndorsements[uid]) {
					var itemid = revid.split('r');
					itemid = itemid[0].split('i');
					itemid = itemid[1];
					if (!Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]) {
						Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid] = Array();
					}
					// define revision ids
					if (!Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid]) {
						Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid] = Array();
					}
					
					for (var action in userEndorsements[uid][revid]) {
						// set the author uid for the revision item
						if (!Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid][action]) {
							Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid][action] = Array();
						}
						if (!Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid][action][uid]) {
							Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid][action][uid] = '';
						}
						Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid][revid][action][uid] = userEndorsements[uid][revid][action];
						
					}
				}
				Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid].sort();
				
			} 
			/*
			else {
				
				if (typeof Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid] != 'undefined'
					&& typeof Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid] != 'undefined') {
					// unset the endorse uid for the revision item
					if (consensus_item.children('.user_data-endorse').text() == 1) {
						// unset the endorse uid
						for (i in Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['endorse']) {
							if (Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['endorse'][i] == uid) {
								delete Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['endorse'][i];
							}
						}
						if (Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['endorse'].length == 0 ) {
							delete Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['endorse'];
						}
					}
					
					if (consensus_item.children('.user_data-author').text() == 1) {
						// unset the author information if the user is the author
						if (typeof Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['author'] !=  'undefined' 
						&& Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['author'] == uid) {
							
							delete Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid]['author'];
						}
					}
					
					
					// unset empty items
					if (Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid].length == 0) {
						delete Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid]['i'+itemid +'r'+ revid];					
					}
					if (Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid].length == 0) {
						delete Drupal.behaviors.consensus.prototype.contributions['c_n'+ nid +'item'+ itemid];
					}
				}
				
			}
			*/
		
		
			if (checked) {
				// set the user id of contributors array
				var found = false;
				for (var i in Drupal.behaviors.consensus.prototype.contributors) {
					if (Drupal.behaviors.consensus.prototype.contributors[i] == uid) {
						found = true
					}
				}
				if (!found) {
					Drupal.behaviors.consensus.prototype.contributors.push(uid);
				}
			} else {
				//unset the uid in the contributors array
				for (var i in Drupal.behaviors.consensus.prototype.contributors) {
					if (Drupal.behaviors.consensus.prototype.contributors[i] == uid) {
						delete Drupal.behaviors.consensus.prototype.contributors[i];
					}
				}
			}
		}
};


/*
*	function builds an array of items with endorse/author information for each revision on the page
*/
Drupal.behaviors.consensus.prototype.updateItemContributions = function(context) {
	var consensus_items = $('.consensus_item', context);
	
	for (i=0; i<consensus_items.length; i++) {	
		Drupal.behaviors.consensus.prototype.updateItem($(consensus_items[i]));
	}
};


Drupal.behaviors.consensus.prototype.updateItem = function (consensus_item) {
	var itemid = consensus_item.attr('id');
	
	// set the array
	if (!(Drupal.behaviors.consensus.prototype.itemContributions[itemid] instanceof Array)) {
			Drupal.behaviors.consensus.prototype.itemContributions[itemid] = Array();
	}
	// add the type of item
	Drupal.behaviors.consensus.prototype.itemContributions[itemid]['type'] = consensus_item.find('> .revision.current .data	.data-type').text();

	// set the blocked attribute
	if (consensus_item.hasClass('c_blocked')) {
		Drupal.behaviors.consensus.prototype.itemContributions[itemid]['blocked'] = Array();
		
		var endorses = consensus_item.find('.c_block > .revision .data .data-endorse');
		
		for (j=0; j<endorses.length; j++) {
			Drupal.behaviors.consensus.prototype.itemContributions[itemid]['blocked']['uid-'+$(endorses[j]).attr('data-uid')] = true;
		}
		
	}
}
	
/*
*	function iterates through each revision item to check whether it should display the revision or not.
*/
Drupal.behaviors.consensus.prototype.includeExclude = function () {
	function sortRevId(a,b)
	{
		a = a.split('r'); 
		a = Number(a[1]);
		b = b.split('r'); 
		b = Number(b[1]);
		
		return b - a;
	}
	function keys(obj)
	{
			var keys = [];
			for(var key in obj)
			{
					keys.push(key);
			}
			keys.sort(sortRevId);
			return keys;
	}
	// check if the blocked box is checked
	if (typeof(toolbox_settings) != 'undefined') {
		var show_blocked = toolboxIsChecked('blocked');
	} else {
		var show_blocked = false;
	}
	for (var itemid in Drupal.behaviors.consensus.prototype.itemContributions) {
		var blocked = false;
		
		// check that item is not blocked by an included contributor
		if (typeof(Drupal.behaviors.consensus.prototype.itemContributions[itemid]['blocked']) != 'undefined' && !show_blocked) {
			for (var i in Drupal.behaviors.consensus.prototype.contributors) {
				if (typeof(Drupal.behaviors.consensus.prototype.itemContributions[itemid]['blocked'][Drupal.behaviors.consensus.prototype.contributors[i]]) != 'undefined') {
					blocked = true;
					$('#'+itemid).addClass('c_blocked');
					break;
				}
			}
			if (!blocked) {
				$('#'+itemid).removeClass('c_blocked');
			}
		}
		
		// check that the item does have at least one endorsement
		var endorsement = false;
		for (i in Drupal.behaviors.consensus.prototype.contributions[itemid]) {
			if (typeof(Drupal.behaviors.consensus.prototype.contributions[itemid][i]['endorse']) != 'undefined') {
				endorsement = true;
				break;
			}
		}
		// ignore filtering rules for headings
		if (Drupal.behaviors.consensus.prototype.itemContributions[itemid]['type'] == 'heading') {
			
		}		
		// check item exists in the included list
		else if (typeof(Drupal.behaviors.consensus.prototype.contributions[itemid]) != 'undefined' && endorsement) {				
			$('#'+itemid).removeClass('exclude').addClass('include').children('.revision').removeClass('top').removeClass('include').addClass('exclude');			

			var revisions = Drupal.behaviors.consensus.prototype.contributions[itemid];
			var revids = keys(Drupal.behaviors.consensus.prototype.contributions[itemid]);
			// iterate through the array and only show visible
			
			var found = false;
			for (var key in revids) {
				if (typeof(revisions[revids[key]]['endorse']) != 'undefined') {
					//$('#'+itemid).removeClass('c_blocked');			
					$('#'+revids[key]).removeClass('exclude').addClass('include');
					found = true;
					
				}
			}
			$('#'+itemid).children('.revision.include').last().addClass('top');
			if (!found) {
				// hide entire item
				$('#'+itemid).removeClass('include').addClass('exclude');
			}
		} else {
			// hide entire item
			$('#'+itemid).removeClass('include').addClass('exclude').children('.revision').removeClass('include').addClass('exclude');
		}
	}
	Drupal.behaviors.consensus.prototype.updateContext();
};

/**
 *	Adds context-in and context-out classes to revision items
 */
Drupal.behaviors.consensus.prototype.updateContext = function () {
	var revisionIds = Drupal.behaviors.consensus.prototype.revisionIds;
	for (i=0; i<revisionIds.length; i++) {
		// see if the item was approved in the existing context
		var revision = $('#'+revisionIds[i]);
		if (revision.hasClass('top')) {
			var revid = revisionIds[i];
			var consensus_item = revision.closest('.consensus_item')
			var itemid = consensus_item.attr('id');
			
			var temp = consensus_item.parent().closest('.consensus_item').children('.revision.top').attr('id');
			if (typeof temp != 'undefined') {
				temp = temp.split('r');
				var parentRevId  = temp[1];
				temp = temp[0].split('i');
				var parentItemId = temp[1];
			} else {
				var parentItemId  = '0';
				var parentRevId = '0';
			}
			
			
			var contextMatch = false;
			if (typeof Drupal.behaviors.consensus.prototype.contributions[itemid] != 'undefined' &&
			typeof Drupal.behaviors.consensus.prototype.contributions[itemid][revid] != 'undefined' &&
			typeof Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse'] != 'undefined') {
				outer:
				for (var uid in Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse']) {
					for (var j in Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse'][uid]) {
						//console.log(Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse'][uid][i]);
						if (Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse'][uid][j]['parentId'] == parentItemId &&
						Drupal.behaviors.consensus.prototype.contributions[itemid][revid]['endorse'][uid][j]['parentRevId'] == parentRevId) {
							contextMatch = true;
							
							break outer;
						}																																																 
					}
				}
			}
			
			if (contextMatch) {
				revision.removeClass('context-out').addClass('context-in');
			} else {
				revision.removeClass('context-in').addClass('context-out');
			}
		}
		
	}
}


/*
 *	Makes the users list sortable
 */
Drupal.behaviors.consensus.prototype.usersSortable = function () {
	$('ul.consensus_user').sortable({
			items: 'li.c_user',
			axis: 'y',
			//containment: 'parent',
			//forceHelperSize: true, // may not be necessary
			//forcePlaceholderSize: true,
			//helper: 'original',
			//helper: 'clone', // if the display is better iwth clone
			handle: '.c_user_wrapper',
			//placeholder: 'c_user_placeholder',
			//revert: true,
			tolerance: 'intersect',
			opacity: 0.6,
			//sort:  function(event, ui) {	
			//	console.log('starting');
			//},
			update:  function(event, ui) {	
				ui.item.find('.collapsible').addClass('stop_collapse');
				$('input[name="sort-order"]').val($(this).sortable('serialize'));
				$('input[name="sortby"]').val('custom');
			},
	
	});
};

Drupal.behaviors.consensus.prototype.toggleFieldset = function(fieldset) {
	if ($(fieldset).is('.collapsed')) {
		// Action div containers are processed separately because of a IE bug
		// that alters the default submit button behavior.
		var content = $('> div:not(.action)', fieldset);
		$(fieldset).removeClass('collapsed');
		content.hide();
		content.slideDown( {
			duration: 'fast',
			easing: 'linear',
			complete: function() {
				Drupal.behaviors.consensus.prototype.collapseScrollIntoView(this.parentNode);
				this.parentNode.animating = false;
				$('div.action', fieldset).show();
				Drupal.behaviors.consensus.prototype.minimizeVal(fieldset);

			},
			step: function() {
				// Scroll the fieldset into view
				Drupal.behaviors.consensus.prototype.collapseScrollIntoView(this.parentNode);
			}
		});
	}
	else {
		$('div.action', fieldset).hide();
		var content = $('> div:not(.action)', fieldset).slideUp('fast', function() {
			$(this.parentNode).addClass('collapsed');
			this.parentNode.animating = false;
			Drupal.behaviors.consensus.prototype.minimizeVal(fieldset);
		});
	}
};

	
/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.behaviors.consensus.prototype.collapseScrollIntoView = function (node) {
	// TODO: this function screws up AJAX
	return false;
	var h = self.innerHeight || document.documentElement.clientHeight || $('body')[0].clientHeight || 0;
	var offset = self.pageYOffset || document.documentElement.scrollTop || $('body')[0].scrollTop || 0;
	var posY = $(node).offset().top;
	var fudge = 55;
	if (posY + node.offsetHeight + fudge > h + offset) {
		if (node.offsetHeight > h) {
			window.scrollTo(0, posY);
		} else {
			window.scrollTo(0, posY + node.offsetHeight - h + fudge);
		}
	}
};



Drupal.behaviors.consensus.prototype.collapse = function (context) {
	$('fieldset.collapsible > legend:not(.collapse-processed)', context).each(function() {
		var fieldset = $(this.parentNode);
		// Expand if there are errors inside
		if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
			fieldset.removeClass('collapsed');
		}

		// Turn the legend into a clickable link and wrap the contents of the fieldset
		// in a div for easier animation
		var text = this.innerHTML;
			$(this).empty().append($('<a href="#">'+ text +'</a>').click(function() {
				var fieldset = $(this).parents('fieldset:first')[0];
				// Don't animate multiple times
				if (!fieldset.animating) {
					fieldset.animating = true;
					Drupal.behaviors.consensus.prototype.toggleFieldset(fieldset);
				}
				return false;
			}))
			.after($('<div class="fieldset-wrapper"></div>')
			.append(fieldset.children(':not(legend):not(.action)')))
			.addClass('collapse-processed');
	});
};


// makes the sortby buttons clickable
Drupal.behaviors.consensus.prototype.usersSortOrder = function (sortby) {
	if (Drupal.settings.consensus.uid != 0) {
		$('#c_user_sortby a').each(function(){
			$(this).removeAttr('href');
			$(this).click(function(){
				$('input[name="sortby"]').val($(this).attr('name'));
				$('#consensus-users form input[type="submit"]').click();
			});
		});
	}
}

